<?php
/**
 * Template part for displaying header part
 *
 * @package domotell
 */

?>

<header id="header">
	<div class="container">
		<div class="logo"><a href="<?php echo get_home_url(); ?>"><img
						src="<?php echo get_template_directory_uri() . '/assets/img/logo_new.png' ?>"
						alt="Site logo"></a></div>
		<div class="header-phones">
			<ul>
				<?php
				$phones = carbon_get_theme_option( 'crb_phones' );
				foreach ( $phones as $phone ) {
					echo '<li>' . $phone['phone'] . '</li>';
				}
				?>
			</ul>
			<a href="#consultation" id="header-consultation" class="popup-with-zoom-anim zakaz">Зворотний дзвінок</a>
		</div>

		<?php
		wp_nav_menu(
			[
				'theme_location'  => 'menu-1',
				'menu_id'         => 'primary-menu',
				'container'       => 'nav',
				'container_class' => 'nav-right',
				'container_id'    => 'nav',
			]
		);
		?>
	</div>
</header>
