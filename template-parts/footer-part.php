<div class="hidden">
    <form class="form-consultation popup-form mfp-hide" id="consultation" method="post" action="<?php echo get_template_directory_uri() . '/inc/mail.php' ?>">
        <h4>Мы готовы Вам помочь!</h4>
        <label>
            <span>Ваше имя:</span>
            <input id="name" name="name" required pattern="[А-Яа-я A-Za-z]{1,32}" title="Формат: Иван Иванов"
                   placeholder="Введите Ваше имя..." required>
        </label>
        <label>
            <span>Ваш телефон:</span>
            <input class="phone_mask" id="tel" name="phone" required title="Формат: 0671234567" placeholder=""
                   required>
        </label>
        <div class="text-center">
            <button type="submit" class="send-button">Консультация!</button>
        </div>
    </form>
</div>
<div class="contacts-mobile"></div>
<div class="big-menu-mobile">
    <div class="big-menu-mobile-header">
        <div class="big-menu-mobile-close">X</div>
    </div>
    <div class="big-menu-mobile-container">
        <p class="big-menu-mobile-title">Нажмите на номер чтобы позвонить:</p>

        <ul>
            <?php
            $phones = carbon_get_theme_option('crb_phones');
            foreach ( $phones as $phone ) {
                echo '<li><a href="' .  $phone['phone'] .'"contacts-block>' . $phone['phone'] . '</a></li>';
            }
            ?>
        </ul>
        <a href="#consultation" id="mobile-consultation" class="big-menu-mobile-button popup-with-zoom-anim zakaz">Обратный
            звонок</a>
    </div>
</div>