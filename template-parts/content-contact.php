<div class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2566.815782266068!2d36.30030221571358!3d49.95855807941056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41270a58f194ee87%3A0xc94e9bb515ad3be0!2z0L_RgNC-0YHQvy4g0JvRjNCy0LAg0JvQsNC90LTQsNGDLCAxNTUsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2MTAwMA!5e0!3m2!1sru!2sua!4v1581609910815!5m2!1sru!2sua"
			width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<div class="position">
	<div class="contacts-block">
		<div class="contacts"><i
					class="icon alt fa-map-marker">&nbsp&nbsp<?php echo carbon_get_theme_option( 'crb_address' ) ?></i>
		</div>
		<div class="telephone">
			<?php
			$phones = carbon_get_theme_option( 'crb_phones' );
			foreach ( $phones as $phone ) {
				echo '<i class="fa-phone icon alt">&nbsp&nbsp' . $phone['phone'] . '</i>';
			}
			?>
		</div>
	</div>
</div>

<section id="five" class="wrapper2 style1 special fade-up">
	<div class="container">
		<header>
			<?php echo wpautop( carbon_get_theme_option( 'crb_footer_text' ) ) ?>

		</header>
		<div class="content-button-banner">
			<p><a href="#consultation" class="button special popup-with-zoom-anim" id="footer-consultation"
				  data-form="Нижняя форма на главной странице со скидкой 10%">Замовити консультацію</a></p>
		</div>
	</div>
</section>