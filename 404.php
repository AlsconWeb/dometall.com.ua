<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package domotell
 */

get_header( 'pool' );
?>

	<div id="fullpage">
		<div class="section " id="section0">
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="main_title upd">
							<h1>Такої сторінки немає на нашому сайті</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="content m-t-b">
				<div class="row">
					<div class="col-md-12">
						<div class="first_link">
							<h1>Перейдіть на <a href="https://dometall.com.ua"><span>Головну</span></a></h1>
						</div>
					</div>
				</div>
			</div>

			<div class="content m-t-b">
				<div class="row">
					<div class="col-md-12">
						<h1>Знайдіть нас на <a href="https://dometall.com.ua/contact"><span>Карті</span></a></h1>
					</div>
				</div>
			</div>

			<div class="content m-t-b">
				<div class="row">
					<div class="col-md-12">
						<h1>Подивіться <a href="https://dometall.com.ua/our-works"><span>Галерею наших робіт</span></a>
						</h1>
					</div>
				</div>
			</div>

		</div>
	</div>

<?php
get_footer();
