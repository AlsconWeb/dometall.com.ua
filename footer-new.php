<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package domotell
 */

?>

<section id="map-screen" class="map-screen">
	<div class="container">
		<h3>ЯК НАС ЗНАЙТИ</h3>
		<div class="left">
			<h4>Наші контакти</h4>

			<ul>
				<li class="location">м. Харків, проспект Льва Ландау, 155-Б</li>
				<?php
				$phones = carbon_get_theme_option( 'crb_phones' );
				foreach ( $phones as $phone ) {
					echo '<li class="phone"><a href="tel:' . esc_attr( $phone['phone'] ) . '">' . esc_html( $phone['phone'] ) . '</a></li>';
				}
				?>
			</ul>

			<h4>Потрібна швидка консультація спеціаліста?</h4>
			<p>Замовте швидку консультацію та отримайте знижку 10% на всі види металоконструкцій </p>
			<a href="#contact-modal" rel="modal:open" class="btn">Замовити консультацію</a>
		</div>
		<div class="right">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2566.815782266068!2d36.30030221571358!3d49.95855807941056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41270a58f194ee87%3A0xc94e9bb515ad3be0!2z0L_RgNC-0YHQvy4g0JvRjNCy0LAg0JvQsNC90LTQsNGDLCAxNTUsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2MTAwMA!5e0!3m2!1sru!2sua!4v1581609910815!5m2!1sru!2sua"
					width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>
</section><!-- map-screen -->

<footer id="footer" class="footer">
	<div class="top">
		<div class="container">
			<div class="column">
				<a href="<?php echo get_home_url(); ?>" class="logo">
					<img src="<?php echo get_template_directory_uri() . '/assets/img/logo_new.png' ?>"
						 alt="Logo Dommetal">
				</a>
				<p>(с) Виробництво металоконструкцій “Дом Металу”</p>
			</div>

			<div class="column">
				<h6><?php esc_html_e( 'Час роботи:', 'dometall' ); ?></h6>

				<p>Пн-пт: 09:00 - 19:00,<br>Сб-нд: вихідні</p>
			</div>

			<div class="column">
				<h6><?php esc_html_e( 'Приймаємо до оплати:', 'dometall' ); ?></h6>
				<span>
                    <img class="img-pay" src="<?php echo get_template_directory_uri() . '/assets/img/icon-visa.png' ?>"
						 alt="Logo visa">
                    <img class="img-pay master"
						 src="<?php echo get_template_directory_uri() . '/assets/img/icon-master.png' ?>"
						 alt="Logo mastercard">
                </span>

				<img class="img-pay privat"
					 src="<?php echo get_template_directory_uri() . '/assets/img/icon-privat.png' ?>"
					 alt="Privat privat24">
			</div>

			<div class="column">
				<h6><?php esc_html_e( 'Ми в соц. мережах:', 'dometall' ); ?></h6>

				<div class="socials">
					<a href="<?php echo carbon_get_theme_option( 'crb_facebook' ) ?>" target="_blank"
					   class="facebook icon-box"></a>
					<a href="<?php echo carbon_get_theme_option( 'crb_instagram' ) ?>" target="_blank"
					   class="instagram icon-box"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom">
		<?php
		wp_nav_menu(
			[
				'theme_location'  => 'menu-1',
				'menu_id'         => 'primary-menu',
				'container'       => 'nav',
				'container_class' => 'nav-right',
				'container_id'    => 'nav',
			]
		);
		?>
	</div>
</footer><!-- #footer -->

<a href="#contact-modal" rel="modal:open" class="zakaz">
	<div class="circlephone" style="transform-origin: center;"></div>
	<div class="circle-fill" style="transform-origin: center;"></div>
	<div class="img-circle" style="transform-origin: center;">
		<div class="img-circleblock" style="transform-origin: center;"></div>
	</div>
</a>

<?php wp_footer(); ?>

</body>
</html>
