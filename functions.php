<?php
/**
 * domotell functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package domotell
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '2.1.0' );
}

if ( ! function_exists( 'dometall_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function dometall_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on domotell, use a find and replace
		 * to change 'dometall' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'dometall', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1' => esc_html__( 'Primary', 'dometall' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'dometall_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);
	}
endif;
add_action( 'after_setup_theme', 'dometall_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dometall_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dometall_content_width', 640 );
}

add_action( 'after_setup_theme', 'dometall_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dometall_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'dometall' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'dometall' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'dometall_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function dometall_scripts() {

	wp_enqueue_style( 'dometall-style-slick', get_template_directory_uri() . '/assets/slick/slick.css', [], _S_VERSION );
	wp_enqueue_style( 'dometall-style-slick-theme', get_template_directory_uri() . '/assets/slick/slick-theme.css', [], _S_VERSION );

	wp_enqueue_style( 'modal-style', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css', [], _S_VERSION );
	wp_enqueue_style( 'dometall-style', get_stylesheet_uri(), [], _S_VERSION );

	wp_enqueue_style( 'dometall-style-main', get_template_directory_uri() . '/sass/style/main.css', [], _S_VERSION );
	wp_enqueue_style( 'dometall-new-global-style', get_template_directory_uri() . '/assets/css/global.css', [], _S_VERSION );


	if ( is_page_template( 'templates/template-rails.php' ) || is_page_template( 'templates/template-railings.php' ) || is_page_template( 'templates/template-fences.php' ) ) {
		wp_enqueue_style( 'dometall-style-rails', get_template_directory_uri() . '/assets/css/rails.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-pool.php' ) ) {
		wp_enqueue_style( 'dometall-full', get_template_directory_uri() . '/sass/style/full.css', [], _S_VERSION );
		wp_enqueue_style( 'dometall-fullpage', get_template_directory_uri() . '/sass/style/fullpage.css', [], _S_VERSION );
		wp_enqueue_style( 'dometall-add_style_pool', get_template_directory_uri() . '/sass/style/add_style_pool.css', [], _S_VERSION );

		wp_enqueue_script( 'dometall-fullpage', get_template_directory_uri() . '/assets/js/fullpage.js', [], _S_VERSION, true );
	}

	if ( is_page_template( 'templates/template-success.php' ) ) {
		wp_enqueue_style( 'dometall-404', get_template_directory_uri() . '/sass/style/404.css', [], _S_VERSION );
	}

	if ( is_page_template( '404.php' ) ) {
		wp_enqueue_style( 'dometall-404', get_template_directory_uri() . '/sass/style/404.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-new.php' ) ) {
		wp_enqueue_style( 'dometall-new-main-style', get_template_directory_uri() . '/assets/css/style.css', [], _S_VERSION );

	}


	if ( ! is_page_template( 'templates/template-new.php' ) || ! is_page_template( 'templates/template-about.php' ) ) {
//        wp_enqueue_script('dometall-form-submission-handler', get_template_directory_uri() . '/assets/js/form-submission-handler.js', array('jquery'), _S_VERSION, true);
		wp_enqueue_script( 'dometall-slick-js', get_template_directory_uri() . '/assets/slick/slick.js', [ 'jquery' ], _S_VERSION, true );

//        wp_enqueue_script('dometall-form-submission-handler', get_template_directory_uri() . '/assets/js/form-submission-handler.js', array('jquery'), _S_VERSION, true);
//        wp_enqueue_script('dometall-bootstrap-min', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), _S_VERSION, true);

		wp_enqueue_script( 'dometall-scrollup', get_template_directory_uri() . '/assets/js/scrollup.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-stock', get_template_directory_uri() . '/assets/js/stock.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-jquery-scrolly-min', get_template_directory_uri() . '/assets/js/jquery.scrolly.min.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-jquery-scrollex-min', get_template_directory_uri() . '/assets/js/jquery.scrollex.min.js', [ 'jquery' ], _S_VERSION, true );


		wp_enqueue_script( 'dometall-util', get_template_directory_uri() . '/assets/js/util.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-mixitup-min', get_template_directory_uri() . '/assets/js/mixitup.min.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-skel-min', get_template_directory_uri() . '/assets/js/skel.min.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-commons', get_template_directory_uri() . '/assets/js/commons.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-jquery-magnific-popup-min', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'dometall-common', get_template_directory_uri() . '/assets/js/common.js', [ 'jquery' ], _S_VERSION, true );

		wp_enqueue_script( 'dometall-main', get_template_directory_uri() . '/assets/js/main.js', [ 'jquery' ], _S_VERSION, true );
	}


	wp_enqueue_script( 'dometall-jquery-maskedinput-min', get_template_directory_uri() . '/assets/js/jquery.maskedinput.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'modal-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'dometall-script', get_template_directory_uri() . '/assets/js/script.js', [], _S_VERSION, true );


	if ( is_page_template( 'templates/template-works.php' ) ) {
		wp_enqueue_style( 'dometall-addstyle', get_template_directory_uri() . '/sass/style/addstyle.css', [], _S_VERSION );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


}

add_action( 'wp_enqueue_scripts', 'dometall_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Add custom fields.
 */
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	Container::make( 'theme_options', __( 'Theme Options' ) )
		->add_tab( __( 'Contacts' ), [
			Field::make( 'complex', 'crb_phones', __( 'Phones' ) )
				->add_fields( [
					Field::make( 'text', 'phone', __( 'Phone' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_address', __( 'Address' ) )
				->set_width( 50 ),
		] )
		->add_tab( __( 'Social links' ), [
			Field::make( 'text', 'crb_instagram', __( 'Instagram' ) )
				->set_width( 33 ),
			Field::make( 'text', 'crb_facebook', __( 'Facebook' ) )
				->set_width( 33 ),
			Field::make( 'text', 'crb_youtube', __( 'Youtube' ) )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Footer' ), [
			Field::make( 'rich_text', 'crb_footer_text', __( 'Footer text' ) )
				->set_default_value( '<h2>Быстрая консультация специалиста</h2><p>Закажите быструю консультацию и получите скидку 10% на все виды металлоконструкций</p>' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Feedbacks' ), [
			Field::make( 'complex', 'crb_feedbacks', __( 'Feedbacks' ) )
				->add_fields( [
					Field::make( 'text', 'name', __( 'Name' ) ),
					Field::make( 'rich_text', 'text', __( 'Text' ) ),
				] ),
		] );
}


add_action( 'carbon_fields_register_fields', 'crb_attach_post_options' );
function crb_attach_post_options() {
	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-home.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_title', __( 'Title' ) )
				->set_default_value( 'Производство и монтаж лестниц и металоконструкций под заказ' )
				->set_width( 50 ),
			Field::make( 'complex', 'crb_lists', __( 'List' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'List item' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_title_first', __( 'Title first' ) )
				->set_default_value( '<span>12</span> років на ринку' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_second', __( 'Title second' ) )
				->set_default_value( "<span>105</span> об'єктів" )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_third', __( 'Title third' ) )
				->set_default_value( '<span>5</span> постачальників' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_fourth', __( 'Title fourth' ) )
				->set_default_value( '<span>288</span> клієнтів' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( '12 років успішної роботи на ринку металоконструкцій' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( "Виконання робіт на об'єктах різної складності" )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'Співпраця з перевіреними та надійними партнерами' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'Задоволені клієнти – наша відповідальність' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Second screen' ), [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Gallery item' ) )
						->set_value_type( 'url' ),
				] )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_column_first', __( 'Column first' ) )
				->set_default_value( 'Мы занимаемся изготовлением лестниц, перил, ограждений из нержавеющей стали, стекла, дерева и гранита. Внимание! Также мы специализируемся на изготовлении кенгурятников, дистилляторов и скупов' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_second', __( 'Column second' ) )
				->set_default_value( 'Цель работы нашего предприятия – в полной мере донести до Вас высочайшие эстетические и эксплуатационные качества металлоизделий, воплощенные в нашей продукции.' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_third', __( 'Column third' ) )
				->set_default_value( 'Налаженная система производства позволяет в сжатые сроки выполнять стандартные заказы, а также реализовывать индивидуальные проекты. Особый подход к каждому клиенту сделают наше с Вами сотрудничество не только результативным, но и приятным.' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'text', 'crb_services_first', __( 'Service first' ) )
				->set_default_value( 'Лестницы <span>для бассейна</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_second', __( 'Service second' ) )
				->set_default_value( 'Внутренние <span>лестницы</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_third', __( 'Service third' ) )
				->set_default_value( 'Металлические <span>ограждения</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_fourth', __( 'Service fourth' ) )
				->set_default_value( 'Изготовление <span>металлоконструкций</span>' )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_first', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_second', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_third', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_fourth', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_first', __( 'Link text' ) )
				->set_default_value( 'Делаем стильно' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_second', __( 'Link text' ) )
				->set_default_value( 'Стремимся к совершенству' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_third', __( 'Link text' ) )
				->set_default_value( 'Думаем о безопасности' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_fourth', __( 'Link text' ) )
				->set_default_value( 'Любим металл' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Fourth screen' ), [
			Field::make( 'textarea', 'crb_benefits_title', __( 'Title' ) )
				->set_default_value( 'Преимущества работы с нами' )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_benefit_first', __( 'Benefit first' ) )
				->set_default_value( 'ИНДИВИДУАЛЬНАЯ СИСТЕМА СКИДОК ДЛЯ КАЖДОГО КЛИЕНТА' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_second', __( 'Benefit second' ) )
				->set_default_value( 'МАКСИМАЛЬНО БЫСТРЫЕ СРОКИ ИЗГОТОВЛЕНИЯ ЛЕСТНИЦ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_third', __( 'Benefit third' ) )
				->set_default_value( 'БЕЗНАЛИЧНАЯ ФОРМА ОПЛАТЫ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_fourth', __( 'Benefit fourth' ) )
				->set_default_value( '12 МЕСЯЦЕВ ГАРАНТИИ НА ИЗДЕЛИЯ ИЗ МЕТАЛЛОКОНСТРУКЦИЙ' )
				->set_width( 25 ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-new.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_title', __( 'Title' ) )
				->set_default_value( 'Производство и монтаж лестниц и металоконструкций под заказ' )
				->set_width( 50 ),
			Field::make( 'complex', 'crb_lists', __( 'List' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'List item' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_title_first', __( 'Title first' ) )
				->set_default_value( '<span>12</span> років на ринку' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_second', __( 'Title second' ) )
				->set_default_value( "<span>105</span> об'єктів" )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_third', __( 'Title third' ) )
				->set_default_value( '<span>5</span> постачальників' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_fourth', __( 'Title fourth' ) )
				->set_default_value( '<span>288</span> клієнтів' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( '12 років успішної роботи на ринку металоконструкцій' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( "Виконання робіт на об'єктах різної складності" )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'Співпраця з перевіреними та надійними партнерами' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'Задоволені клієнти – наша відповідальність' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'text', 'crb_services_first', __( 'Service first' ) )
				->set_default_value( 'Лестницы <span>для бассейна</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_second', __( 'Service second' ) )
				->set_default_value( 'Внутренние <span>лестницы</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_third', __( 'Service third' ) )
				->set_default_value( 'Металлические <span>ограждения</span>' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_services_fourth', __( 'Service fourth' ) )
				->set_default_value( 'Изготовление <span>металлоконструкций</span>' )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_first', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_second', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_third', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'association', 'crb_s_link_fourth', __( 'Link' ) )
				->set_max( 1 )
				->set_types( [
					[
						'type'      => 'post',
						'post_type' => 'page',
					],
				] )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_first', __( 'Link text' ) )
				->set_default_value( 'Делаем стильно' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_second', __( 'Link text' ) )
				->set_default_value( 'Стремимся к совершенству' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_third', __( 'Link text' ) )
				->set_default_value( 'Думаем о безопасности' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_s_l_text_fourth', __( 'Link text' ) )
				->set_default_value( 'Любим металл' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Fourth screen' ), [
			Field::make( 'textarea', 'crb_f_title', __( 'Title' ) )
				->set_default_value( 'ключевые кредо компании дом металла' )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_f_first', __( 'Benefit first' ) )
				->set_default_value( 'Полный спектр услуг!' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_f_second', __( 'Benefit second' ) )
				->set_default_value( 'Миссия нашей компании ' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_f_third', __( 'Benefit third' ) )
				->set_default_value( 'Наши особенности и подход к работе ' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_f_first_t', __( 'Benefit first' ) )
				->set_default_value( 'Мы занимаемся не только изготовлением лестниц, перил, ограждений из нержавеющей стали, стекла, дерева и гранита. Мы также специализируемся на изготовлении кенгурятников, дистилляторов и скупов.' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_f_second_t', __( 'Benefit second' ) )
				->set_default_value( 'Цель работы нашего предприятия – в полной мере донести до Вас высочайшие эстетические и эксплуатационные качества металлоизделий, воплощенные в нашей продукции.' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_f_third_t', __( 'Benefit third' ) )
				->set_default_value( 'Налаженная система производства позволяет в сжатые сроки выполнять стандартные заказы, а также реализовывать индивидуальные проекты. Особый подход к каждому клиенту сделают наше с Вами сотрудничество не только результативным, но и приятным.' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Fifth screen' ), [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Gallery item' ) )
						->set_value_type( 'url' ),
				] )
				->set_width( 100 ),
		] )->add_tab( __( 'Six screen', 'dometall' ), [
			Field::make( 'complex', 'crb_partners', __( 'Partners', 'dometall' ) )
				->add_fields( [
					Field::make( 'image', 'logo', __( 'Partners logo', 'dometall' ) )
						->set_value_type( 'url' ),
					Field::make( 'text', 'crb_link', __( 'Link to partner', 'dometall' ) ),
				] )
				->set_width( 100 ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-fences.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_title', __( 'Title' ) )
				->set_default_value( 'Закажите индивидуальное ограждение у ведущего производителя Харьковской области с опытом 12 лет!' )
				->set_width( 50 ),
			Field::make( 'complex', 'crb_lists', __( 'List' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'List item' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_title_first', __( 'Title first' ) )
				->set_default_value( '<span>12</span> років на ринку' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_second', __( 'Title second' ) )
				->set_default_value( "<span>105</span> об'єктів" )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_third', __( 'Title third' ) )
				->set_default_value( '<span>5</span> постачальників' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_fourth', __( 'Title fourth' ) )
				->set_default_value( '<span>288</span> клієнтів' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( '12 років успішної роботи на ринку металоконструкцій' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( "Виконання робіт на об'єктах різної складності" )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'Співпраця з перевіреними та надійними партнерами' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'Задоволені клієнти – наша відповідальність' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Second screen' ), [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Gallery item' ) )
						->set_value_type( 'url' ),
				] )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_column_first', __( 'Column first' ) )
				->set_default_value( 'Мы занимаемся изготовлением лестниц, перил, ограждений из нержавеющей стали, стекла, дерева и гранита. Внимание! Также мы специализируемся на изготовлении кенгурятников, дистилляторов и скупов' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_second', __( 'Column second' ) )
				->set_default_value( 'Цель работы нашего предприятия – в полной мере донести до Вас высочайшие эстетические и эксплуатационные качества металлоизделий, воплощенные в нашей продукции.' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_third', __( 'Column third' ) )
				->set_default_value( 'Налаженная система производства позволяет в сжатые сроки выполнять стандартные заказы, а также реализовывать индивидуальные проекты. Особый подход к каждому клиенту сделают наше с Вами сотрудничество не только результативным, но и приятным.' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'textarea', 'crb_benefits_title', __( 'Title' ) )
				->set_default_value( 'Преимущества работы с нами' )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_benefit_first', __( 'Benefit first' ) )
				->set_default_value( 'ИНДИВИДУАЛЬНАЯ СИСТЕМА СКИДОК ДЛЯ КАЖДОГО КЛИЕНТА' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_second', __( 'Benefit second' ) )
				->set_default_value( 'МАКСИМАЛЬНО БЫСТРЫЕ СРОКИ ИЗГОТОВЛЕНИЯ ЛЕСТНИЦ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_third', __( 'Benefit third' ) )
				->set_default_value( 'БЕЗНАЛИЧНАЯ ФОРМА ОПЛАТЫ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_fourth', __( 'Benefit fourth' ) )
				->set_default_value( '12 МЕСЯЦЕВ ГАРАНТИИ НА ИЗДЕЛИЯ ИЗ МЕТАЛЛОКОНСТРУКЦИЙ' )
				->set_width( 25 ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-railings.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_title', __( 'Title' ) )
				->set_default_value( 'Закажите индивидуальное ограждение у ведущего производителя Харьковской области с опытом 12 лет!' )
				->set_width( 50 ),
			Field::make( 'complex', 'crb_lists', __( 'List' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'List item' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_title_first', __( 'Title first' ) )
				->set_default_value( '<span>12</span> років на ринку' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_second', __( 'Title second' ) )
				->set_default_value( "<span>105</span> об'єктів" )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_third', __( 'Title third' ) )
				->set_default_value( '<span>5</span> постачальників' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_fourth', __( 'Title fourth' ) )
				->set_default_value( '<span>288</span> клієнтів' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( '12 років успішної роботи на ринку металоконструкцій' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( "Виконання робіт на об'єктах різної складності" )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'Співпраця з перевіреними та надійними партнерами' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'Задоволені клієнти – наша відповідальність' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Second screen' ), [
			Field::make( 'complex', 'crb_types', __( 'Types' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Type image' ) )
						->set_width( 50 ),
					Field::make( 'text', 'text', __( 'Type text' ) )
						->set_width( 50 ),
				] ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Gallery item' ) )
						->set_value_type( 'url' ),
				] )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_column_first', __( 'Column first' ) )
				->set_default_value( 'Мы занимаемся изготовлением лестниц, перил, ограждений из нержавеющей стали, стекла, дерева и гранита. Внимание! Также мы специализируемся на изготовлении кенгурятников, дистилляторов и скупов' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_second', __( 'Column second' ) )
				->set_default_value( 'Цель работы нашего предприятия – в полной мере донести до Вас высочайшие эстетические и эксплуатационные качества металлоизделий, воплощенные в нашей продукции.' )
				->set_width( 33 ),
			Field::make( 'textarea', 'crb_column_third', __( 'Column third' ) )
				->set_default_value( 'Налаженная система производства позволяет в сжатые сроки выполнять стандартные заказы, а также реализовывать индивидуальные проекты. Особый подход к каждому клиенту сделают наше с Вами сотрудничество не только результативным, но и приятным.' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'Fourth screen' ), [
			Field::make( 'textarea', 'crb_benefits_title', __( 'Title' ) )
				->set_default_value( 'Преимущества работы с нами' )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_benefit_first', __( 'Benefit first' ) )
				->set_default_value( 'ИНДИВИДУАЛЬНАЯ СИСТЕМА СКИДОК ДЛЯ КАЖДОГО КЛИЕНТА' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_second', __( 'Benefit second' ) )
				->set_default_value( 'МАКСИМАЛЬНО БЫСТРЫЕ СРОКИ ИЗГОТОВЛЕНИЯ ЛЕСТНИЦ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_third', __( 'Benefit third' ) )
				->set_default_value( 'БЕЗНАЛИЧНАЯ ФОРМА ОПЛАТЫ' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_benefit_fourth', __( 'Benefit fourth' ) )
				->set_default_value( '12 МЕСЯЦЕВ ГАРАНТИИ НА ИЗДЕЛИЯ ИЗ МЕТАЛЛОКОНСТРУКЦИЙ' )
				->set_width( 25 ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-works.php' )
		->add_fields( [
			Field::make( 'complex', 'crb_cat_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'Category name' ) )
						->set_width( 30 ),
					Field::make( 'media_gallery', 'crb_media_gallery', __( 'Media Gallery' ) )
						->set_width( 70 ),
				] ),
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'Category name' ) )
						->set_width( 50 ),
					Field::make( 'complex', 'images', __( 'Images' ) )
						->add_fields( [
							Field::make( 'image', 'img', __( 'Image' ) )
								->set_value_type( 'url' ),
						] )
						->set_width( 50 ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-metal.php' )
		->add_fields( [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Image' ) )
						->set_value_type( 'url' ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-pool.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_screen_first', __( 'Text' ) )
				->set_default_value( 'Лестница для бассейна из нержавеющей стали используются для входа и выхода из бассейна, находит применение в бассейнах небольшого размера для экономии полезного пространства и дополнительного комфорта.' )
				->set_width( 100 ),
		] )
		->add_tab( __( 'Second screen' ), [
			Field::make( 'textarea', 'crb_screen_second', __( 'Text' ) )
				->set_default_value( 'Лестницы отличаются по количеству ступеней, точкам крепления и выносу поручня. Ступени имеют анатомическую форму и структурированную противоскользящую поверхность' )
				->set_width( 100 ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'text', 'crb_screen_third', __( 'Title' ) )
				->set_default_value( 'КАКИЕ ПАРАМЕТРЫ УЧИТЫВАТЬ ПРИ ВЫБОРЕ ЛЕСТНИЦЫ В БАССЕЙН?' )
				->set_width( 100 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( 'МАТЕРИАЛ ИЗГОТОВЛЕНИЯ' )
				->set_width( 15 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( 'КОЛИЧЕСТВО СТУПЕНЕК' )
				->set_width( 15 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'РАССТОЯНИЕ МЕЖДУ СТУПЕНЬКАМИ' )
				->set_width( 15 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'ГИГИЕНИЧНОСТЬ' )
				->set_width( 15 ),
			Field::make( 'textarea', 'crb_text_fifth', __( 'Text fifth' ) )
				->set_default_value( 'КАЧЕСТВО ПОКРЫТИЯ' )
				->set_width( 15 ),
			Field::make( 'textarea', 'crb_text_six', __( 'Text six' ) )
				->set_default_value( 'КАЧЕСТВО ИЗГОТОВЛЕНИЯ ПОРУЧНЕЙ' )
				->set_width( 15 ),
		] );

	Container::make( 'post_meta', __( 'Page Options' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-rails.php' )
		->add_tab( __( 'First screen' ), [
			Field::make( 'textarea', 'crb_title', __( 'Title' ) )
				->set_default_value( 'Закажите индивидуальную лестницу у ведущего производителя Харьковской области с опытом 12 лет!' )
				->set_width( 50 ),
			Field::make( 'complex', 'crb_lists', __( 'List' ) )
				->add_fields( [
					Field::make( 'text', 'text', __( 'List item' ) ),
				] )
				->set_width( 50 ),
			Field::make( 'text', 'crb_title_first', __( 'Title first' ) )
				->set_default_value( '<span>12</span> років на ринку' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_second', __( 'Title second' ) )
				->set_default_value( "<span>105</span> об'єктів" )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_third', __( 'Title third' ) )
				->set_default_value( '<span>5</span> постачальників' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_title_fourth', __( 'Title fourth' ) )
				->set_default_value( '<span>288</span> клієнтів' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_first', __( 'Text first' ) )
				->set_default_value( '12 років успішної роботи на ринку металоконструкцій' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_second', __( 'Text second' ) )
				->set_default_value( "Виконання робіт на об'єктах різної складності" )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_third', __( 'Text third' ) )
				->set_default_value( 'Співпраця з перевіреними та надійними партнерами' )
				->set_width( 25 ),
			Field::make( 'textarea', 'crb_text_fourth', __( 'Text fourth' ) )
				->set_default_value( 'Задоволені клієнти – наша відповідальність' )
				->set_width( 25 ),
		] )
		->add_tab( __( 'Second screen' ), [
			Field::make( 'text', 'title11', __( 'Title 2' ) ),
			Field::make( 'complex', 'crb_types', __( 'Types' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Type image' ) )
						->set_width( 50 ),
					Field::make( 'text', 'text', __( 'Type text' ) )
						->set_width( 50 ),
				] ),
		] )
		->add_tab( __( 'Third screen' ), [
			Field::make( 'text', 'title12', __( 'Title 1' ) ),
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'image', 'img', __( 'Gallery item' ) )
						->set_value_type( 'url' ),
				] )
				->set_width( 100 ),
		] );
}

/**
 * Add support svg.
 *
 * @param array $file_types file mine-type.
 *
 * @return mixed
 */
function add_svg_support( array $file_types ) {
	$file_types['svg'] = 'image/svg+xml';

	return $file_types;
}

add_filter( 'upload_mimes', 'add_svg_support' );