<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package domotell
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"
			integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP"
			crossorigin="anonymous"></script>

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=cyrillic" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header id="header">
	<div class="container">
		<div class="left">
			<div class="header-phones">
				<ul>
					<?php
					$phones = carbon_get_theme_option( 'crb_phones' );
					foreach ( $phones as $phone ) {
						echo '<li>' . $phone['phone'] . '</li>';
					}
					?>
				</ul>
				<a href="#contact-modal" rel="modal:open" class="btn">Зворотний дзвінок</a>
			</div>
		</div>
		<div class="center">
			<div class="logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_template_directory_uri() . '/assets/img/logo_new.png' ?>" alt="Site logo">
				</a>
			</div>
		</div>
		<div class="right">
			<?php
			wp_nav_menu(
				[
					'theme_location'  => 'menu-1',
					'menu_id'         => 'primary-menu',
					'container'       => 'nav',
					'container_class' => 'nav-right',
					'container_id'    => 'nav',
				]
			);
			?>

			<div class="burger-menu">
				<a href="" class="burger-menu_button">
					<spun class="burger-menu_lines"></spun>
					<spun class="burger-menu_lines"></spun>
					<spun class="burger-menu_lines"></spun>
				</a>
				<nav class="burger-menu_nav">
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'menu-1',
						]
					);
					?>
				</nav>
				<div class="burger-menu_overlay"></div>
			</div>

		</div>
	</div>
</header><!-- #header -->

<div id="contact-modal" class="modal">
	<h5><?php echo esc_html_e( 'Нужен звонок?', 'art-cube' ); ?></h5>
	<div class="form-box">
		<?php echo do_shortcode( '[contact-form-7 id="749" title="Contact form"]' ) ?>
	</div>
</div>
