<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package domotell
 */

?>

<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div>(с) Виробництво металоконструкцій “Дом Металу”</div>
				<div>Час роботи:</div>
				<div>Пн-пт: 9:00-18:00</div>
				<div>Сб-нд: вихідні</div>

				<?php
				$phones = carbon_get_theme_option( 'crb_phones' );
				foreach ( $phones as $phone ) {
					echo '<div><a href="' . $phone['phone'] . '">' . $phone['phone'] . '</a></div>';
				}
				?>

			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<h3>Корисне:</h3>
				<ul>
					<li><a href="https://dometall.com.ua/feddback/">Відгуки</a></li>
					<li><a href="https://dometall.com.ua/garant/">Гарантія</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<h3>Приймаємо до оплати</h3>
				<div class="pay"><img src="https://img.dometall.com.ua/ps-p24.png"></div>
				<div class="pay"><img src="https://img.dometall.com.ua/ps-visa.png"></div>
				<div class="pay"><img src="https://img.dometall.com.ua/ps-maca.png"></div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<ul class="icons">
					<li>
						<a href="<?php echo carbon_get_theme_option( 'crb_facebook' ) ?>"
						   class="icon alt fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
					<li><a href="<?php echo carbon_get_theme_option( 'crb_instagram' ) ?>" class="icon alt fa-instagram"
						   target="_blank"><span
									class="label">Instagram</span></a></li>
					<!--                    <li><a href="-->
					<?php //echo carbon_get_theme_option('crb_youtube') ?><!--" class="icon alt fa-youtube-play" target="_blank"><span-->
					<!--                                    class="label">Мы - здесь</span></a></li>-->
				</ul>
			</div>
		</div>
	</div>

	<a id="popup__toggle" href="#consultation" class="popup-with-zoom-anim zakaz">
		<div class="circlephone" style="transform-origin: center;"></div>
		<div class="circle-fill" style="transform-origin: center;"></div>
		<div class="img-circle" style="transform-origin: center;">
			<div class="img-circleblock" style="transform-origin: center;"></div>
		</div>
	</a>

</footer>
<div class="button-mobile">
	<div id="rpb">
		<div>
			<a href="tel:0684993267" id="call_now"
			   onclick=" ga('send', 'event', 'Phone Call', 'Click to Call', '0684993267'); ">
				<span class="dashicons dashicons-phone"></span> Дзвінок
			</a>
		</div>
		<div>
			<a href="https://goo.gl/maps/tsmyXVmwPUDhwHQTA" id="map_now" target="_Blank">
				<span class="dashicons dashicons-location"></span> Мапа
			</a>
		</div>
	</div>
</div>
<div class="butom-mess">
</div>

</div>

<?php wp_footer(); ?>
<?php get_template_part( 'template-parts/footer', 'part' ); ?>


<script>
	jQuery( '.phone_mask' ).mask( '+38(999)999-99-99' );
</script>


<script type="text/javascript">
	jQuery( document ).ready( function() {
		jQuery( '.mobile-hidden' ).slick( {
			dots: false,
			infinite: true,
			speed: 100,
			fade: true,
			cssEase: 'linear',
			autoplay: true
		} );
	} );
</script>
</body>
</html>
