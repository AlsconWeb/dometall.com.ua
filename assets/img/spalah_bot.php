<?php

require_once("lib/telegram_bot.php");


class TestBot extends TelegramBot{

	//protected $token = "919333226:AAFrSHUtEFcCYui0Hn8lj4_47I0meme9yjc";
	//protected $bot_name = "@testbottestbottestbot_bot";
	//public $proxy = "tcp://185.67.1.16:80";

	
	protected $commands = [
			"/start" => "cmd_start",
			"/help" => "cmd_help",
			"/human" => "cmd_human"

		];

	/**
	 *
	 * https://core.telegram.org/bots/api#replykeyboardmarkup
	 * 
	 */
	public $keyboards = [
		'inline' => [
			// Две кнопки в ряд
			[
				// вызовет метод callback_act1(),
				[
					'text' => "Маркетинг",
					'callback_data'=> "act1"
				],
				
				[
					'text' => "Дизайн",
					'callback_data'=> "act2"
				]
			],
			[
				['text' => "Управление", 'callback_data'=> "act3"],
				['text' => "Программирование", 'callback_data'=> "act4"]
			],
			// Кнопка на всю ширину
			[
				['text' => "Бесплатные курсы", 'callback_data'=> "act5"],
			]
		],
		'back' =>[[['text' => "↩ Назад", 'callback_data'=> "back"]]
	],

	'marketing' =>[
		[
			[				
				'text' => "Email маркетинг, как инструмент вовлечения и удержания", 
				'callback_data'=> "act6"			
			],

			[				
				'text' => "Построение системы digital маркетинга для бизнеса", 
				'callback_data'=> "act7"			
			]
		],					

			[
				['text' => "↩ Назад к выбору направления обучения", 'callback_data'=> "back"]
			]				
		],

	'anekdot' =>[
		[
			[				
				'text' => "Анекдот", 
				'callback_data'=> "act27"			
			]
		]	
	],

	'noanekdot' =>[
		[
			[				
				'text' => "↩ Начать всё сначала", 
				'callback_data'=> "back"			
			]
		]	
	],

	'free' =>[
		[
			[				
				'text' => "Основы баз данных (SQL)", 
				'callback_data'=> "act19"			
			],
	
			[				
				'text' => "Основы Python", 
				'callback_data'=> "act20"			
			]
		],					
	
		[
			[
				'text' => "↩ Назад", 'callback_data'=> "back"
			]
		]				
	],

	'design' =>[
		[
			[				
				'text' => "Основы дизайна интерфейсов (UI/UX)", 
				'callback_data'=> "act10"			
			],

			[				
				'text' => "Креативное мышление в дизайне", 
				'callback_data'=> "act11"			
			]
		],					

			[
				['text' => "↩ Назад", 'callback_data'=> "back"]
			]				
		],

		'management' =>[
			[
				[				
					'text' => "Управление персоналом", 
					'callback_data'=> "act12"			
				],
	
				[				
					'text' => "Управление проектами и командой", 
					'callback_data'=> "act13"			
				]
			],
			
			[
				[				
					'text' => "Стартапы, финансы и бизнес", 
					'callback_data'=> "act14"			
				]
			],				
	
				[
					['text' => "↩ Назад", 'callback_data'=> "back"]
				]				
			],

		'hr' =>[
			[
				[				
					'text' => "HR - Управление персоналом в IT", 
					'callback_data'=> "act15"			
				],
			
				[				
					'text' => "People Management: навыки управления людьми", 
					'callback_data'=> "act16"			
				]
			],
			
			[
				[				
					'text' => "Управление персоналом на основе стратегии бизнеса", 
					'callback_data'=> "act17"			
				]
			],				
			
				[
					['text' => "↩ Назад", 'callback_data'=> "backmanagement"]
				]				
			],

			'projects' =>[
				[
					[				
						'text' => "PM - Управление проектами в IT", 
						'callback_data'=> "act22"			
					]
				],
				
				[
					['text' => "↩ Назад", 'callback_data'=> "backmanagement"]
				]				
			],

			'bussines' =>[
				[
					[				
						'text' => "Создание IT-стартапа на глобальный рынок", 
						'callback_data'=> "act23"			
					],
					[				
						'text' => "Управление дизайн-студией", 
						'callback_data'=> "act24"			
					],

				],
				[
					[				
						'text' => "Финансовое управление для сервисной IT-компании", 
						'callback_data'=> "act25"			
					],
					[				
						'text' => "Навыки успешных переговоров", 
						'callback_data'=> "act28"			
					],

				],
				
				[
					['text' => "↩ Назад", 'callback_data'=> "backmanagement"]
				]				
			],

			'startup' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backbussines"]
				]				
			],



			'basepm' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backprojects"]
				]				
			],

		'basehr' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backhr"]
				]				
			],

		'strategyhr' =>[				
			[
				['text' => "↩ Назад", 'callback_data'=> "backhr"]
			]				
		],

		'peoplem' =>[				
			[
				['text' => "↩ Назад", 'callback_data'=> "backhr"]
			]				
		],

		'email' =>[				
			[
				['text' => "↩ Назад", 'callback_data'=> "backmarketing"]
			]				
		],

		'digital' =>[				
			[
				['text' => "↩ Назад", 'callback_data'=> "backmarketing"]
			]				
		],

		'program' =>[
			[
				[				
					'text' => "Основы баз данных (SQL)", 
					'callback_data'=> "act8"		
				],
			
				[				
					'text' => "QA Manual - Тестирование ПО", 
					'callback_data'=> "act18"			
				]
			],
			
			[
				[				
					'text' => "Основы Python", 
					'callback_data'=> "act9"			
				]
			],				
			
				[
					['text' => "↩ Назад", 'callback_data'=> "back"]
				]				
			],

			'sql' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backprogram"]
				]				
			],

			'python' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backprogram"]
				]				
			],

			'qa' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backprogram"]
				]				
			],

			'sqlfree' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backfree"]
				]				
			],

			'pythonfree' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backfree"]
				]				
			],

			'uiux' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backdesign"]
				]				
			],

			'creative' =>[				
				[
					['text' => "↩ Назад", 'callback_data'=> "backdesign"]
				]				
			],

	


];

	
	/**
	 * Обработка ввода команды "/start"
	 */
	function cmd_start(){
		$this->api->sendMessage([
			'text'=>"<b>Выбери направление для обучения или задай вопрос боту:</b>",
			'parse_mode'=> 'HTML',
			'reply_markup' => json_encode( [
				'inline_keyboard'=> $this->keyboards['inline']
			] )
		]);
	}

	function cmd_help(){
		
		$this->api->sendMessage([
			'text' => "Мы рядом:\n " . implode("\n ", array_keys( $this->commands ) ) . "",
			'parse_mode'=> 'HTML'
		]);
	}

	function cmd_human(){
		
		$this->api->sendMessage([
			'text' => "Человек здесь: @SpalahOnline",
			'parse_mode'=> 'HTML'
		]);
	}

	/**
	 * Обработка ввода команды "Назад в главное меню"
	 */
	function callback_back(){
		$text = "<b>Выбери направление для обучения или задай вопрос боту:</b>";
		$this->callbackAnswer( $text, $this->keyboards['inline'] );
	}

	/**
	 * Обработка ввода команды "Назад в менеджмент"
	 */
	function callback_backmanagement(){
		$text = "m a n a g e m e n t\n\nЕсли ты только начинаешь свою профессиональную карьеру или уже являешься состоявшимся собственником бизнеса - мы поможем в построении карьеры и в оптимальном решении задач бизнеса!\n\n<b>Давай детальнее определимся с направлением:</b>";
		$this->callbackAnswer( $text, $this->keyboards['management'] );
	}

	/**
	 * Обработка ввода команды "Назад в HR"
	 */
	function callback_backhr(){
		$text = "Данные курсы призваны сопровождать тебя от <b>трудоустройства</b> на должность HR менеджера до создания эффектвной <b>системы управления персоналом!</b>\n \n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['hr'] );
	}

	/**
	 * Обработка ввода команды "Назад в PM"
	 */
	function callback_backprojects(){
		$text = "<b>Проджект менеджер</b> - ключевая роль в функционировании любой IT компании.\n\nТебе сейчас доступен <b>один</b> курс в данном направлении, но мы уже работаем над новыми.\n \n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['projects'] );
	}

	/**
	 * Обработка ввода команды "Назад в Маркетинг"
	 */
	function callback_backmarketing(){
		$url = "https://esputnik.com";
		$url1 = "https://netpeak.ua";
		$url2 = "https://owox.ua";
		$text = "m a r k e t i n g\n\nВ создании курсов направления принимают участие сотрудники компаний <b>eSputnik</b>, <b>Netpeak</b>, <b>Owox</b> \n\n$url\n\n$url1\n\n$url2\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['marketing'] );
	}

	/**
	 * Обработка ввода команды "Назад в Программирование"
	 */
	function callback_backprogram(){
		$text = "p r o g r a m m i n g\n\nВход в IT тернист, а порог входа на Junior вакансии стабильно повышается.\n\nИменно поэтому мы создали программы курсов, которые направлены на твоё последующее трудоустройство.\n\n✦Курс <b>Основы баз данных (SQL)</b> полностью доступен <b>для бесплатного обучения!</b>\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['program'] );
	}

	/**
	 * Обработка ввода команды "Назад в Дизайн"
	 */
	function callback_backdesign(){
		$url = "https://vintage.com.ua";
		$url1 = "https://nixsolutions.com";
		$text = "d e s i g n\n\nСлоган направления: <b>Стань крутым дизайнером!</b>\n\nКурс <b>Основы дизайна интерфейсов</b> создавался при участии компании <b>NixSolutions</b>.\n\nНа десерт тебя ждёт <b>Креативное мышление в веб-дизайне</b> – авторский курс Ольги Шевченко – арт-директора лучшей дизайн-студии Украины <b>Vintage Web Prodaction</b>! \n\n$url\n\n$url1\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['design'] );
	}

	/**
	 * Обработка ввода команды "Назад в Бесплатные курсы"
	 */
	function callback_backfree(){
		$text = "f r e e\n\nКурс <b>Основы БД</b> полностью бесплатен для самостоятельного прохождения! \n \nКаждому студенту <b>Основы Python</b> доступны для бесплатного прохождения первые 6 уроков!\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['free'] );
	}

	/**
	 * Обработка ввода команды "Назад в Бизнес"
	 */
	function callback_backbussines(){
		$text = "Авторы этих курсов – безусловно <b>успешные предприниматели</b>. Но вcегда ли они были на <b>волне успеха</b>, всегда ли их <b>идеи работали</b>, а проекты <b>приносили прибыль?</b> (<em>Вопрос риторический</em>).\n\nЭто не просто очередные курсы '<em>про бизнес.</em>'\nЭто квинтэссенция <b>опыта</b>, ошибочных <b>решений</b> и правильных <b>выводов</b>.\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['bussines'] );
	}

	/**
	 * Обработка ввода команды "Маркетинг"
	 */
	function callback_act1(){
		$url = "https://esputnik.com";
		$url1 = "https://netpeak.ua";
		$url2 = "https://owox.ua";
		$text = "m a r k e t i n g\n\nВ создании курсов направления принимали участие сотрудники компаний <b>eSputnik</b>, <b>Netpeak</b>, <b>Owox</b> \n\n$url\n\n$url1\n\n$url2\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['marketing'] );
	}

	/**
	 * Обработка ввода команды "Дизайн"
	 */
	function callback_act2(){
		$url = "https://vintage.com.ua";
		$url1 = "https://nixsolutions.com";
		$text = "d e s i g n\n\nСлоган направления: <b>Стань крутым дизайнером!</b>\n\nКурс <b>Основы дизайна интерфейсов</b> создавался при участии компании <b>NixSolutions</b>.\n На десерт тебя ждёт <b>Креативное мышление в веб-дизайне</b> – авторский курс Ольги Шевченко – арт-директора лучшей дизайн-студии Украины <b>Vintage Web Prodaction</b>! \n\n$url\n\n$url1\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['design'] );
	}

	/**
	 * Обработка ввода команды "Управление"
	 */
	function callback_act3(){
		$text = "m a n a g e m e n t\n\nЕсли ты только <b>начинаешь свою профессиональную карьеру</b> или уже являешься состоявшимся <b>собственником бизнеса</b> - мы поможем в построении карьеры и в оптимальном решении задач твоего бизнеса!\n\n<b>Давай детальнее определимся с направлением:</b>";
		$this->callbackAnswer( $text, $this->keyboards['management'] );
	}

	/**
	 * Обработка ввода команды "Программирование"
	 */
	function callback_act4(){
		$text = "p r o g r a m m i n g\n\nВход в IT тернист, а порог входа на Junior вакансии стабильно повышается.\n\nИменно поэтому мы создали программы курсов, которые направлены на твоё последующее трудоустройство.\n\n✦Курс <b>Основы баз данных (SQL)</b> полностью доступен <b>для бесплатного обучения!</b>\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['program'] );
	}

	/**
	 * Обработка ввода команды "Бесплатные курсы"
	 */
	function callback_act5(){
		$text = "f r e e\n\nКурс <b>Основы БД</b> полностью бесплатен для самостоятельного прохождения! \n \nКаждому студенту <b>Основы Python</b> доступны для бесплатного прохождения первые 6 уроков!\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['free'] );
	}

	/**
	 * Обработка ввода команды поднаправления "HR"
	 */
	function callback_act12(){
		$text = "Данные курсы призваны сопровождать тебя от <b>трудоустройства</b> на должность HR менеджера до создания тобой эффективной <b>системы управления персоналом!</b>\n \n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['hr'] );
	}

	/**
	 * Обработка ввода команды поднаправления "PM"
	 */
	function callback_act13(){
		$text = "<b>Проджект менеджер</b> - ключевая роль в функционировании любой IT компании.\n\nТебе сейчас доступен <b>один</b> курс в данном направлении, но мы уже работаем над новыми.\n \n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['projects'] );
	}


	/**
	 * Обработка ввода команды поднаправления "Bussines"
	 */
	function callback_act14(){
		$text = "Авторы этих курсов – безусловно <b>успешные предприниматели</b>. Но вcегда ли они были на <b>волне успеха</b>, всегда ли их <b>идеи работали</b>, а проекты <b>приносили прибыль?</b> (<em>Вопрос риторический</em>).\n\nЭто не просто очередные курсы '<em>про бизнес.</em>'\nЭто квинтэссенция <b>опыта</b>, ошибочных <b>решений</b> и правильных <b>выводов</b>.\n\n<b>Выбери курс:</b>";
		$this->callbackAnswer( $text, $this->keyboards['bussines'] );
	}

	/**
	 * Обработка ввода команды "Базовый PM"
	 */
	function callback_act22(){
		$url = "https://spalah.com/course/pm#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Курс создан для:</b>\n- руководителей небольших команд;\n- тестировщиков;\n- программистов и собственников небольших студий для более глубокого понимания процессов управления командой и проектами.\n\n<b>Главная цель курса:</b>\nДать необходимые знания и практические навыки для трудоустройства на должность Junior Project Manager\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['basepm'] );
	}

	/**
	 * Обработка ввода команды "Базовый HR"
	 */
	function callback_act15(){
		$url = "https://spalah.com/course/hr#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Курс создан для:</b>\n- HR менеджеров работающих с персоналом не в IT-сфере;\n- начинающих HR-ов из профильных к IT-индустрии областях;\n- для людей, которые стремятся стать HR менеджерами с профессиональной ориентацией на IT-бизнес.\n\n<b>Главная цель курса:</b>\nОбеспечить вход в профессию (трудоустройство) на должность HR менеджера в IT компанию!\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['basehr'] );
	}

	/**
	 * Обработка ввода команды "Управление персоналом на основе стратегии бизнеса"
	 */
	function callback_act17(){
		$url = "https://spalah.com/course/hr-business-process#formatsHighlight";
		$urls = "https://www.youtube.com/watch?v=Vd4N9EOz9Y4";
		$text = "<em>Сразу хочу разочаровать всех желающих получить волшебную пилюлю во время курса.\n\nПридётся получать знания, практиковаться и нарабатывать навыки. Тогда всё получится.</em> – <b>Анна Власова, профессор Киевской школы экономики, кандидат экономических наук,</b> <a href='" . $urls . "'>автор курса.</a>\n\n<b>Уровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\nТоп менеджеров и опытных HR специалистов, которые стремятся поставить процесс управления персоналом в соответствии с бизнес-стратегией.\n\n<b>Главная цель курса:</b>\nНаучить разрабатывать HR стратегию и создавать эффективную систему управления персоналом, основанную на целях бизнеса.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['strategyhr'] );
	}

	/**
	 * Обработка ввода команды "People Management: навыки управления людьми"
	 */
	function callback_act16(){
		$url = "https://spalah.com/course/people-management#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★★★\n\n<b>Курс создан для:</b>\n- CEO (Генеральных директоров);\n- представителей топ-менеджмента;\n- линейных руководителей, которые вовлечены в управление организацией.\n\n<b>Главная цель курса:</b>\nРазвить навыки использования инструментов управления организацией и научить выявлять факторы влияющие на эффективность управленческих решений.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['peoplem'] );
	}

	/**
	 * Обработка ввода команды "Email маркетинг, как инструмент вовлечения и удержания"
	 */
	function callback_act6(){
		$url = "https://spalah.com/course/e-mail-marketing#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\n- практикующих маркетологов;\n- руководителей E-commerce проектов;\n- C-level менеджеров;\n- владельцев и сотрудников маркетинговых агентств.\n\n<b>Главная цель курса:</b>\nНаучить находить и отслеживать ключевые показатели по удержанию и вовлечению пользователей.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['email'] );
	}

	/**
	 * Обработка ввода команды "Построение системы digital маркетинга для бизнеса"
	 */
	function callback_act7(){
		$url = "https://spalah.com/course/marketing-for-business#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Курс создан для:</b>\n- технических специалистов отдела маркетинга;\n- руководителей и топ-менеджеров компаний, у которых есть отделы маркетинга, но эффективность их работы не поддаётся объективной оценке.\n\n<b>Главная цель курса:</b>\nНаучить выявлять ключевые показатели эффективности работы отдела маркетинга, настраивать их отслеживание, контроль и прогнозирование.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['digital'] );
	}

	/**
	 * Обработка ввода команды "Основы баз данных (SQL)"
	 */
	function callback_act8(){
		$url = "https://spalah.com/course/sql#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Инструменты:</b>\n✓ SQLYog;\n✓ MySQL Workbench;\n✓ phpMyAdmin.\n\n<b>Курс создан для:</b>\n- начинающих тестировщиков для перехода в сферу автоматизированного тестирования;\n- front-end и back-end разработчиков, которые собираются использовать реляционные базы данных в реальных проектах.\n\n<b>Главная цель курса:</b>\nПомочь начинающим специалистам в прохождении собеседований на должности, которые предполагают взаимодействия с реляционными базами данных, а также — на систематизацию ранее полученных знаний.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['sql'] );
	}

	/**
	 * Обработка ввода команды "Тестирование ПО (QA Manual)"
	 */
	function callback_act18(){
		$url = "https://spalah.com/course/qa#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★☆☆☆\n\n<b>Курс создан для:</b>\n- стремящихся найти первую работу в IT и выбравших для этого QA направление.\n- начинающих проджект менеджеров и разработчиков для понимания основных процессов обеспечения качества продукта.\n\n<b>Главная цель курса:</b>\nДать необходимые знания и практические навыки для работы на позиции Trainee QA Engineer.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['sql'] );
	}

	/**
	 * Обработка ввода команды "Основы Python"
	 */
	function callback_act9(){
		$url = "https://spalah.com/course/python#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★★☆\n\n<b>Инструменты:</b>\n✓ Python 2.7;\n✓ Python 3.5;\n✓ Веб-фреймворк Django.\n\n<b>Курс создан для:</b>\nНачинающих веб-разработчиков, которые хотят углубиться в изучение back-end составляющей веб-приложений.\n\n<b>Главная цель курса:</b>\nРазвитие навыков для трудоустройства на позицию Trainee Python Developer.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['python'] );
	}

		/**
	 * Обработка ввода команды "Основы Python из Бесплатных курсов"
	 */
	function callback_act20(){
		$url = "https://spalah.com/course/python#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★★☆\n\n<b>Инструменты:</b>\n✓ Python 2.7;\n✓ Python 3.5;\n✓ Веб-фреймворк Django.\n\n<b>Курс создан для:</b>\nНачинающих веб-разработчиков, которые хотят углубиться в изучение back-end составляющей веб-приложений.\n\n<b>Главная цель курса:</b>\nРазвитие навыков для трудоустройства на позицию Trainee Python Developer.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['pythonfree'] );
	}

	/**
	 * Обработка ввода команды "Основы баз данных (SQL) из Бесплатных курсов"
	 */
	function callback_act19(){
		$url = "https://spalah.com/course/sql#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Инструменты:</b>\n✓ SQLYog;\n✓ MySQL Workbench;\n✓ phpMyAdmin.\n\n<b>Курс создан для:</b>\n- начинающих тестировщиков для перехода в сферу автоматизированного тестирования;\n- front-end и back-end разработчиков, которые собираются использовать реляционные базы данных в реальных проектах.\n\n<b>Главная цель курса:</b>\nПомочь начинающим специалистам в прохождении собеседований на должности, которые предполагают взаимодействия с реляционными базами данных, а также — на систематизацию ранее полученных знаний.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['sqlfree'] );
	}

	/**
	 * Обработка ввода команды "Основы дизайна интерфейсов (UI/UX)"
	 */
	function callback_act10(){
		$url = "https://spalah.com/course/ui-ux#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★☆☆\n\n<b>Инструменты:</b>\n✓ Adobe XD;\n✓ Invision;\n✓ Zeplin.\n\n<b>Курс создан для:</b>\n- стремящихся найти первую работу в сфере веб-дизайна;\n- начинающих верстальщиков, веб-разработчиков для расширения навыков и развития в своей профессии.\n\n<b>Главная цель курса:</b>\nДать практические навыки для трудоустройства на должность Trainee/Junior UI/UX Designer.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['uiux'] );
	}

	/**
	 * Обработка ввода команды "Креативное мышление в веб-дизайне"
	 */
	function callback_act11(){
		$url = "https://spalah.com/course/creative-design#formatsHighlight";
		$text = "<em>Этот курс про технологии, анимацию и эмоции, которые сайт должен вызывать</em> – <b>Ольга Шевченко, арт-директор дизайн студии Vintage, автор курса.</b>\n\n<b>Уровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\nСтремящихся узнать больше о том, как правильно делать сайты, придумывать крутые идеи и создавать неординарные проекты.\n\n<b>Главная цель курса:</b>\nНаучить думать как режиссёр, продумывать анимацию, взаимодействие, типографику и нестандартные сетки для создания эмоционального веб-дизайна, который будет получать международные награды. \n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['creative'] );
	}

	/**
	 * Обработка ввода команды "Создание IT-стартапа на глобальный рынок"
	 */
	function callback_act23(){
		$url = "https://spalah.com/course/startup-acceleration#formatsHighlight";
		$urls = "https://ain.ua/2019/05/27/ukrainskij-servis-dlya-faunderov-do-mite-system-privlek-50-000-investicij";
		$text = "<em>Стартап студента первого набора привлёк 50000$ инвестиций после окончания курса</em> – <a href='" . $urls . "'>AIN</a>\n\n<b>Уровень сложности: </b>★★★★★\n\n<b>Курс создан для:</b>\nПредпринимателей и команд, которые имеют проект на ранней стадии и планируют выводить его на глобальный рынок с привлечением инвестиций.\n\n<b>К участию рассматриваются проекты в сфере High Technologies:</b>\nMobile Apps, Lifestyle, Productivity, Sustainable living, AI, VR/AR, Big Data, Smart City, Hardware.\n\n<b>Главная цель курса:</b>\nДать возможность участникам уверенно выступать перед инвесторами, участвовать в международных конференциях, подавать заявки и проходить отбор в акселерационные и инкубационные программы Европы и США.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['startup'] );
	}

	/**
	 * Обработка ввода команды "Управление дизайн студией"
	 */
	function callback_act24(){
		$url = "https://spalah.com/course/web-studio#formatsHighlight";
		$urls = "https://ain.ua/2019/07/15/krokodily-kotorye-ne-puskayut-vash-biznes-razvivatsya-opyt-studii-vintage";
		$text = "<em>От мыслей о закрытии компании до лучшей дизайн-студии в Украине.\n\nИсходя из пройденного нами пути и набитых шишек, мы решили собрать весь наш опыт в один курс, который подходит для любой сервисной компании.</em> – <b>Евгений Кудрявченко в интервью</b> <a href='" . $urls . "'>AIN</a>\n\n<b>Уровень сложности: </b>★★★★★\n\n<b>Курс создан для:</b>\n- дизайнеров и менеджеров, которые планируют создать собственную дизайн-студию;\n- руководителей и владельцев действующих веб-студий, которые сталкиваются с проблемой масштабирования и нестабильной прибыльностью работы студии.\n\n<b>Главная цель курса:</b>\nНаучить эффективно управлять, масштабировать и зарабатывать на дизайн-студии.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['startup'] );
	}

	/**
	 * Обработка ввода команды "Финансовое управление для сервисной IT-компании"
	 */
	function callback_act25(){
		$url = "https://spalah.com/course/it-finance#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\n- собственников сервисных IT-компаний;\n- топ-менеджеров и финансовых аналитиков, которые работают в сервисных IT-компаниях и стремятся усовершенствовать свои знания в области управления финансами и управленческого учета.\n\n<b>Главная цель курса:</b>\nНаучить онтролировать финансовые показатели и планировать бюджеты для повышения финансовой эффективности в сервисных IT-компаниях.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['startup'] );
	}

	/**
	 * Обработка ввода команды "Навыки успешных переговоров"
	 */
	function callback_act28(){
		$url = "https://spalah.com/course/leader-talks#formatsHighlight";
		$text = "<b>Уровень сложности: </b>★☆☆☆☆\n\n<b>Курс создан для:</b>\n- собственников и руководителей;\n- PM и HR менеджеров;\n- заинтересованных в развитии своего собственного лидерского потенциала.\n\n<b>Главная цель курса:</b>\nВыстраивать общение с партнёрами через мотивацию и доверие, не задевая конфликтные зоны, ставить задачи таким образом, чтобы они выполнялись с ответственностью и мотивацией, развивать лидерский потенциал через влияние в переговорах.\n\n<b>Подавай заявку:</b>\n$url";
		$this->callbackAnswer( $text, $this->keyboards['startup'] );
	}

		/**
	 * Обработка ввода команды "Анекдот"
	 */
	function callback_act27(){
			$text = "<em>В действительности всё совсем не так, как на самом деле. (Станислав Ежи Лец)</em>\n\n\nЗаходят десять математиков в бар.\n\n<b>Первый говорит:</b> — Мне пол литра пива, пожалуйста.\n\n<b>Второй говорит:</b> — А мне четверть литра.\n\n<b>Третий:</b> — А мне одну восьмую.\n\n<b>Четвертый:</b> — А мне тоже одну восьмую.\n\n — Ну охуеть теперь! - <b>говорят остальные шесть.</b>\n\n\nЕсли ты уже устал смеяться, давай начнём всё сначала.";
			$this->callbackAnswer( $text, $this->keyboards['noanekdot'] );				 
	}


	/**
	 * Обработка ввода команды "Картинка" отправляет сообщение с картинкой
	 */
	function cmd_kartinka(){
		$this->api->sendPhoto( "https://webportnoy.ru/upload/alno/alno3.jpg", "Описание картинки" );
	}
	

	/**
	 * Обработка ввода команды "Гифка" отправляет сообщение с гифкой
	 */
	function cmd_gifka(){
		$this->api->sendDocument( "https://webportnoy.ru/upload/1.gif", "Описание гифки" );
	}

	/**
	 * Обработка ввода команды "Новости" отправляет сообщение со списком новостей из RSS-ленты
	 */
	function cmd_novosti(){
		$rss = simplexml_load_file('http://vposelok.com/feed/1001/');
		$text = "";
		foreach( $rss->channel->item as $item ){
			$text .= "\xE2\x9E\xA1 " . $item->title . " (<a href='" . $item->link . "'>читать</a>)\n\n";
		}
		$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => true, 
			'text' => $text 
		]);
	}

	/**
	 * Обработка ввода команды "Музыка" отправляет сообщение с аудиофайлом
	 * 20 Mb maximum: https://core.telegram.org/bots/api#sending-files
	 */
	function cmd_music(){
		$url = "http://vposelok.com/files/de-phazz_-_strangers_in_the_night.mp3";
		$this->api->sendAudio( $url );
	}

	/**
	 * Обработка ввода команды "Подкаст" отправляет сообщение с последним выпуском подкаста
	 * Если файл подкаста меньше 20 Мб, то он будет отправлен сообщением, в противном случае будет добавлена ссылка на скачивание.
	 * 20 Mb maximum: https://core.telegram.org/bots/api#sending-files
	 */
	function cmd_podcast(){
		$rss = simplexml_load_file('https://meduza.io/rss/podcasts/tekst-nedeli');

		$item = $rss->channel->item;
		$enclosure = (array) $item->enclosure;
		$size = round( $enclosure['@attributes']['length'] / (1024*1024), 1 );
		$text = "🎙 {$item->title}";

		if( $size < 20 ){
			$this->api->sendAudio( $enclosure['@attributes']['url'] );
		}
		else{
			$text .= "\n\n⬇️ <a href='" . $enclosure['@attributes']['url'] . "'>скачать</a> {$size}Mb";
		}

		$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => true, 
			'text' => $text 
		]);
	}

	/**
	 * Ответ на ввод, не распознанный как команда
	 */
	function cmd_default(){

		if( stripos( $this->result["message"]["text"], "hello world" ) !== false
		|| stripos( $this->result["message"]["text"], "Hello, World" ) !== false 
		|| stripos( $this->result["message"]["text"], "хеллоу вёрлд" ) !== false 
		|| stripos( $this->result["message"]["text"], "helloworld" ) !== false 
		|| stripos( $this->result["message"]["text"], "Привет, мир" ) !== false 
		|| stripos( $this->result["message"]["text"], "привет мир" ) !== false 
		|| stripos( $this->result["message"]["text"], "приветмир" ) !== false 
		|| stripos( $this->result["message"]["text"], "privet mir" ) !== false 
		|| stripos( $this->result["message"]["text"], "pryvetmir" ) !== false 
		|| stripos( $this->result["message"]["text"], "pryvet mir" ) !== false 
		|| stripos( $this->result["message"]["text"], "helo world" ) !== false 
		|| stripos( $this->result["message"]["text"], "helloworld" ) !== false
		){
			$this->api->sendPhoto("http://lurkmore.so/images/e/e6/HelloVitek.jpg", "Лурк(с)");
			}

		elseif( stripos( $this->result["message"]["text"], "такие спалах" ) !== false
		|| stripos( $this->result["message"]["text"], "такой спалах" ) !== false 
		|| stripos( $this->result["message"]["text"], "такой сплэш" ) !== false 
		|| stripos( $this->result["message"]["text"], "такое спалах" ) !== false 
		|| stripos( $this->result["message"]["text"], "такие спалах" ) !== false 
		|| stripos( $this->result["message"]["text"], "Кто вы такие?" ) !== false 
		|| stripos( $this->result["message"]["text"], "кто вы такие?" ) !== false 
		|| stripos( $this->result["message"]["text"], "что за спалах" ) !== false 
		|| stripos( $this->result["message"]["text"], "Что за Spalah" ) !== false 
		|| stripos( $this->result["message"]["text"], "Что за спалах" ) !== false 
		|| stripos( $this->result["message"]["text"], "что у вас за школа?" ) !== false 
		|| stripos( $this->result["message"]["text"], "школа занимается" ) !== false
		|| stripos( $this->result["message"]["text"], "такой spalah" ) !== false 
		|| stripos( $this->result["message"]["text"], "такой Spalah" ) !== false 
		|| stripos( $this->result["message"]["text"], "такое spalah" ) !== false 
		|| stripos( $this->result["message"]["text"], "такие spalah" ) !== false 
		|| stripos( $this->result["message"]["text"], "это вспышка?" ) !== false 
		|| stripos( $this->result["message"]["text"], "spalah что это" ) !== false
		|| stripos( $this->result["message"]["text"], "палах что это" ) !== false
		|| stripos( $this->result["message"]["text"], "кто вы" ) !== false  
		|| stripos( $this->result["message"]["text"], "Кто вы" ) !== false  
	
		){
			$this->api->sendPhoto("https://img.dometall.com.ua/spalah.jpg", "SPALAH – это мы.");
			}
		
		elseif( stripos( $this->result["message"]["text"], "ак дела?" ) !== false
		|| stripos( $this->result["message"]["text"], "ак жизнь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "то ты такой?" ) !== false 
		|| stripos( $this->result["message"]["text"], "то ты?" ) !== false 
		|| stripos( $this->result["message"]["text"], "Что ты такое?" ) !== false 
		|| stripos( $this->result["message"]["text"], "Ты кто?" ) !== false 
		|| stripos( $this->result["message"]["text"], "ты кто?" ) !== false 
		|| stripos( $this->result["message"]["text"], "кто ты по жизни" ) !== false 
		|| stripos( $this->result["message"]["text"], "чей будешь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "ачем ты здесь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "что ты делаешь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "то ты можешь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "how are you?" ) !== false 
		|| stripos( $this->result["message"]["text"], "ты кто такой?" ) !== false
		|| stripos( $this->result["message"]["text"], "ты кто по жизни?" ) !== false
		|| stripos( $this->result["message"]["text"], "ктоты?" ) !== false
		|| stripos( $this->result["message"]["text"], "тыкто?" ) !== false
		|| stripos( $this->result["message"]["text"], "ктоты" ) !== false
		|| stripos( $this->result["message"]["text"], "тыкто" ) !== false
		|| stripos( $this->result["message"]["text"], "ачем ты здесь" ) !== false 
		|| stripos( $this->result["message"]["text"], "что ты делаешь" ) !== false 
		|| stripos( $this->result["message"]["text"], "что ты можешь" ) !== false 
		|| stripos( $this->result["message"]["text"], "кто ты" ) !== false 
		|| stripos( $this->result["message"]["text"], "кто такой?" ) !== false 
		|| stripos( $this->result["message"]["text"], "кто такой" ) !== false 
		|| stripos( $this->result["message"]["text"], "чё как?" ) !== false 
		|| stripos( $this->result["message"]["text"], "чё, как?" ) !== false 
		|| stripos( $this->result["message"]["text"], "вои дела" ) !== false 
		|| stripos( $this->result["message"]["text"], "ачем тебя создали?" ) !== false
		|| stripos( $this->result["message"]["text"], "ачем ты нужен?" ) !== false
		|| stripos( $this->result["message"]["text"], "ачем тебя придумали?" ) !== false
		|| stripos( $this->result["message"]["text"], "ачем ты нужен" ) !== false
		|| stripos( $this->result["message"]["text"], "ачем тебя придумали" ) !== false
		|| stripos( $this->result["message"]["text"], "что ты способен" ) !== false
		|| stripos( $this->result["message"]["text"], "Что ты способен" ) !== false
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			'text' => "\n\nАйм файн!\n\nЖиву на серваке, чувствую себя в корневом каталоге!\n\nЯ создан, чтобы помочь тебе, @" . $this->result["message"]["from"]["username"] . ", выбрать <b>твой путь</b>, осуществить <b>твои мечты</b> и изменить твою <b>жизнь к лучшему</b>!\n\nНикогда не опускай руки и тогда всё получится!\n\nЯ вот тоже иногда туплю (<em>ты уж не обижайся</em>), но не расстраиваюсь и каждый день учусь!"
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "кто я?" ) !== false
		|| stripos( $this->result["message"]["text"], "Кто я?" ) !== false 
		|| stripos( $this->result["message"]["text"], "Я кто?" ) !== false 
		|| stripos( $this->result["message"]["text"], "меня знаешь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "Знаешь меня?" ) !== false 
		|| stripos( $this->result["message"]["text"], "гадай меня" ) !== false 
		|| stripos( $this->result["message"]["text"], "меня угадаешь?" ) !== false 
		|| stripos( $this->result["message"]["text"], "тгадай меня" ) !== false 
		|| stripos( $this->result["message"]["text"], "гадай меня" ) !== false 
		|| stripos( $this->result["message"]["text"], "гадай кто я" ) !== false 
		|| stripos( $this->result["message"]["text"], "ты знаешь с кем разговариваешь" ) !== false 
		|| stripos( $this->result["message"]["text"], "Ты знаешь с кем общаешься" ) !== false 
		|| stripos( $this->result["message"]["text"], "Ты знаешь с кем разговариваешь" ) !== false 
		|| stripos( $this->result["message"]["text"], "ты знаешь с кем общаешься" ) !== false 
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			'text' => "\n\nТы - @" . $this->result["message"]["from"]["username"] . " (но это не точно), будем знакомы!\n\nЕсли ты здесь, значит тебе нужен я - это отлично, я готов (и это тоже не точно)."
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "кто тебя создал" ) !== false
		|| stripos( $this->result["message"]["text"], "тебя создал" ) !== false 
		|| stripos( $this->result["message"]["text"], "тебя придумал" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя закодил" ) !== false 
		|| stripos( $this->result["message"]["text"], "вой создатель" ) !== false 
		|| stripos( $this->result["message"]["text"], "твой творец" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя сотворил" ) !== false 
		|| stripos( $this->result["message"]["text"], "отворить тебя" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя породил" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя родил" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя сваял" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебя написал" ) !== false
		|| stripos( $this->result["message"]["text"], "ебя сделал" ) !== false
		|| stripos( $this->result["message"]["text"], "из говна и палок" ) !== false
		|| stripos( $this->result["message"]["text"], "тебя выдумал" ) !== false
		|| stripos( $this->result["message"]["text"], "тебя запилил" ) !== false
		|| stripos( $this->result["message"]["text"], "твои создатели" ) !== false
		|| stripos( $this->result["message"]["text"], "ебя слепил" ) !== false
		|| stripos( $this->result["message"]["text"], "ебя захуярил" ) !== false
		|| stripos( $this->result["message"]["text"], "ебя сварганил" ) !== false
		|| stripos( $this->result["message"]["text"], "ты был написан" ) !== false
		|| stripos( $this->result["message"]["text"], "был закоден" ) !== false		
		|| stripos( $this->result["message"]["text"], "ебя закодил" ) !== false
		|| stripos( $this->result["message"]["text"], "чей ты" ) !== false
		|| stripos( $this->result["message"]["text"], "из гавна и палок" ) !== false
		){
			$this->api->sendMessage([
				'parse_mode' => 'HTML',
				'disable_web_page_preview' => false, 
				'text' => $text,
				'text' => "\n\nМеня придумал @smirnovdima\n\nЕсли я туплю, значит Дима виноват." 
			]);				 
			}
			
		elseif( stripos( $this->result["message"]["text"], "асскажи анекдот" ) !== false
		|| stripos( $this->result["message"]["text"], "пошути как-то" ) !== false 
		|| stripos( $this->result["message"]["text"], "умеешь шутить" ) !== false 
		|| stripos( $this->result["message"]["text"], "ошути" ) !== false 
		|| stripos( $this->result["message"]["text"], "апиши что-нибудь смешное" ) !== false 
		|| stripos( $this->result["message"]["text"], "апиши что-нибудь" ) !== false 
		|| stripos( $this->result["message"]["text"], "апиши что нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "апиши что нить" ) !== false
		|| stripos( $this->result["message"]["text"], "апиши шо нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "кажи что-нибудь смешное" ) !== false 
		|| stripos( $this->result["message"]["text"], "юбишь юмор?" ) !== false 
		|| stripos( $this->result["message"]["text"], "юмор давай" ) !== false 
		|| stripos( $this->result["message"]["text"], "давай анекдот" ) !== false 
		|| stripos( $this->result["message"]["text"], "некдот" ) !== false 
		|| stripos( $this->result["message"]["text"], "шутку в студию" ) !== false 
		|| stripos( $this->result["message"]["text"], "смешно" ) !== false
		|| stripos( $this->result["message"]["text"], "о чём нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "о чём-нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "поговорим" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи историю" ) !== false
		|| stripos( $this->result["message"]["text"], "смешную" ) !== false
		|| stripos( $this->result["message"]["text"], "шутк" ) !== false	
		|| stripos( $this->result["message"]["text"], "истори" ) !== false	
		|| stripos( $this->result["message"]["text"], "асскажи что то" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи что-то" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи шото" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи шо то" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи что-нибудь смешное" ) !== false 
		|| stripos( $this->result["message"]["text"], "асскажи что-нибудь" ) !== false 
		|| stripos( $this->result["message"]["text"], "асскажи что нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи что нить" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи шо нибудь" ) !== false
		|| stripos( $this->result["message"]["text"], "асскажи мне" ) !== false

		){
			$this->api->sendMessage([
				'parse_mode' => 'HTML',
				'disable_web_page_preview' => false, 
				'text' => $text,
				'text' => "\n\nДавай <b>анекдот</b> расскажу.\n\nНо предупреждаю, что пока знаю <b>только один</b> и тот <b>не для джунов</b>.\n\nПоэтому подумай прежде чем нажимать.\n\nИли нажимай /start, чтобы вернуться и начать всё сначала.",
				'reply_markup' => json_encode( [
					'inline_keyboard'=> $this->keyboards['anekdot']
					] )
			]);				 
			}
		
		elseif( stripos( $this->result["message"]["text"], "email" ) !== false
		|| stripos( $this->result["message"]["text"], "ail" ) !== false 
		|| stripos( $this->result["message"]["text"], "шухари" ) !== false 
		|| stripos( $this->result["message"]["text"], "удренко" ) !== false 
		|| stripos( $this->result["message"]["text"], "пунтик" ) !== false 
		|| stripos( $this->result["message"]["text"], "Кудренко дмитрий" ) !== false 
		|| stripos( $this->result["message"]["text"], "рассылк" ) !== false 
		|| stripos( $this->result["message"]["text"], "спутник" ) !== false 
		|| stripos( $this->result["message"]["text"], "esputnik" ) !== false 
		|| stripos( $this->result["message"]["text"], "e-sputnik" ) !== false 
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/e-mail-marketing#formatsHighlight",
			'text' => "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\n<b>'Email маркетинг, как инструмент вовлечения и удержания'</b><b>\n\nУровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\n- практикующих маркетологов;\n- руководителей E-commerce проектов;\n- C-level менеджеров;\n- владельцев и сотрудников маркетинговых агентств.\n\n<b>Главная цель курса:</b>\nНаучить находить и отслеживать ключевые показатели по удержанию и вовлечению пользователей.\n\n<b>Подавай заявку:</b>\n$url",
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "удрявченко" ) !== false
		|| stripos( $this->result["message"]["text"], "веб студию" ) !== false 
		|| stripos( $this->result["message"]["text"], "веб-студию" ) !== false 
		|| stripos( $this->result["message"]["text"], "дизайн студию" ) !== false 
		|| stripos( $this->result["message"]["text"], "дизайн-студией" ) !== false 
		|| stripos( $this->result["message"]["text"], "крутые сайты" ) !== false 
		|| stripos( $this->result["message"]["text"], "крутые кейсы" ) !== false 
		|| stripos( $this->result["message"]["text"], "оздавать сайты" ) !== false 
		|| stripos( $this->result["message"]["text"], "продакшен" ) !== false 
		|| stripos( $this->result["message"]["text"], "ирового уровня" ) !== false 
		|| stripos( $this->result["message"]["text"], "оздать с нуля" ) !== false
		|| stripos( $this->result["message"]["text"], "веб студи" ) !== false
		|| stripos( $this->result["message"]["text"], "веб-студи" ) !== false
		|| stripos( $this->result["message"]["text"], "web студи" ) !== false
		|| stripos( $this->result["message"]["text"], "web-студи" ) !== false
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/web-studio#formatsHighlight",
			$urls = "https://ain.ua/2019/07/15/krokodily-kotorye-ne-puskayut-vash-biznes-razvivatsya-opyt-studii-vintage",
			'text' => "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\n<b>'Управление дизайн-студией'</b>\n\n<em>От мыслей о закрытии компании до лучшей дизайн-студии в Украине.\n\nИсходя из пройденного нами пути и набитых шишек, мы решили собрать весь наш опыт в один курс, который подходит для любой сервисной компании.</em> – <b>Евгений Кудрявченко в интервью</b> <a href='" . $urls . "'>AIN</a>\n\n<b>Уровень сложности: </b>★★★★★\n\n<b>Курс создан для:</b>\n- дизайнеров и менеджеров, которые планируют создать собственную дизайн-студию;\n- руководителей и владельцев действующих веб-студий, которые сталкиваются с проблемой масштабирования и нестабильной прибыльностью работы студии.\n\n<b>Главная цель курса:</b>\nНаучить эффективно управлять, масштабировать и зарабатывать на дизайн-студии.\n\n<b>Подавай заявку:</b>\n$url",
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "оздавать дизайн" ) !== false
		|| stripos( $this->result["message"]["text"], "рутой дизайн" ) !== false 
		|| stripos( $this->result["message"]["text"], "реативное мышление" ) !== false 
		|| stripos( $this->result["message"]["text"], "wwwards" ) !== false 
		|| stripos( $this->result["message"]["text"], "Авардс" ) !== false 
		|| stripos( $this->result["message"]["text"], "евченко" ) !== false 
		|| stripos( $this->result["message"]["text"], "исовать сайты" ) !== false 
		|| stripos( $this->result["message"]["text"], "рутой дизайн" ) !== false 
		|| stripos( $this->result["message"]["text"], "родвинутый дизайн" ) !== false 
		|| stripos( $this->result["message"]["text"], "родвинутого дизайна" ) !== false 
		|| stripos( $this->result["message"]["text"], "рутое портфолио" ) !== false
		|| stripos( $this->result["message"]["text"], "оздавать портфолио" ) !== false
		|| stripos( $this->result["message"]["text"], "эвардс" ) !== false
		|| stripos( $this->result["message"]["text"], "веб дизайн" ) !== false
		|| stripos( $this->result["message"]["text"], "веб-дизайн" ) !== false
		|| stripos( $this->result["message"]["text"], "крутого дизайна" ) !== false
		|| stripos( $this->result["message"]["text"], "крутым дизайном" ) !== false
		|| stripos( $this->result["message"]["text"], "моциональный дизайн" ) !== false
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/creative-design#formatsHighlight",
			'text' =>  "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\n<b>'Креативное мышление в веб-дизайне'</b>\n\n<em>Этот курс про технологии, анимацию и эмоции, которые сайт должен вызывать</em> – <b>Ольга Шевченко, арт-директор дизайн студии Vintage, автор курса.</b>\n\n<b>Уровень сложности: </b>★★★★☆\n\n<b>Курс создан для:</b>\nСтремящихся узнать больше о том, как правильно делать сайты, придумывать крутые идеи и создавать неординарные проекты.\n\n<b>Главная цель курса:</b>\nНаучить думать как режиссёр, продумывать анимацию, взаимодействие, типографику и нестандартные сетки для создания эмоционального веб-дизайна, который будет получать международные награды. \n\n<b>Подавай заявку:</b>\n$url",
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "стартап" ) !== false
		|| stripos( $this->result["message"]["text"], "Стартап" ) !== false 
		|| stripos( $this->result["message"]["text"], "ривлечение инвестиций" ) !== false 
		|| stripos( $this->result["message"]["text"], "Довгопол" ) !== false 
		|| stripos( $this->result["message"]["text"], "урс про IT бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "IT-бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "кселлератор" ) !== false 
		|| stripos( $this->result["message"]["text"], "кселлера" ) !== false 
		|| stripos( $this->result["message"]["text"], "на глобальный рынок" ) !== false 
		|| stripos( $this->result["message"]["text"], "редпринимател" ) !== false 
		|| stripos( $this->result["message"]["text"], "IT бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "it бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "айти бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "ай ти бизнес" ) !== false
		|| stripos( $this->result["message"]["text"], "it-бизнес" ) !== false 
		|| stripos( $this->result["message"]["text"], "нвестици" ) !== false 
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/startup-acceleration#formatsHighlight",
			$urls = "https://ain.ua/2019/05/27/ukrainskij-servis-dlya-faunderov-do-mite-system-privlek-50-000-investicij",
			'text' => "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\n<b>'Создание IT-стартапа на глобальный рынок'</b>\n\n<em>Стартап студента первого набора привлёк 50000$ инвестиций после окончания курса</em> – <a href='" . $urls . "'>AIN</a>\n\n<b>Уровень сложности: </b>★★★★★\n\n<b>Курс создан для:</b>\nПредпринимателей и команд, которые имеют проект на ранней стадии и планируют выводить его на глобальный рынок с привлечением инвестиций.\n\n<b>К участию рассматриваются проекты в сфере High Technologies:</b>\nMobile Apps, Lifestyle, Productivity, Sustainable living, AI, VR/AR, Big Data, Smart City, Hardware.\n\n<b>Главная цель курса:</b>\nДать возможность участникам уверенно выступать перед инвесторами, участвовать в международных конференциях, подавать заявки и проходить отбор в акселерационные и инкубационные программы Европы и США.\n\n<b>Подавай заявку:</b>\n$url",
			]);
		}		

		elseif( stripos( $this->result["message"]["text"], "digital" ) !== false
		|| stripos( $this->result["message"]["text"], "чумаченко" ) !== false 
		|| stripos( $this->result["message"]["text"], "seo-продвижение" ) !== false 
		|| stripos( $this->result["message"]["text"], "продвижение" ) !== false 
		|| stripos( $this->result["message"]["text"], "оптимизация" ) !== false 
		|| stripos( $this->result["message"]["text"], "seo" ) !== false 
		|| stripos( $this->result["message"]["text"], "netpeak" ) !== false 
		|| stripos( $this->result["message"]["text"], "нетпик" ) !== false 
		|| stripos( $this->result["message"]["text"], "джиджитал" ) !== false 
		|| stripos( $this->result["message"]["text"], "диджитал" ) !== false 
		|| stripos( $this->result["message"]["text"], "медиаплан" ) !== false 
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/marketing-for-business#formatsHighlight",
			'text' => "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\n<b>'Построение системы digital маркетинга для бизнеса'</b> \n\n<b>Уровень сложности: </b>★★★☆☆\n\n<b>Курс создан для:</b>\n- технических специалистов отдела маркетинга;\n- руководителей и топ-менеджеров компаний, у которых есть отделы маркетинга, но эффективность их работы не поддаётся объективной оценке.\n\n<b>Главная цель курса:</b>\nНаучить выявлять ключевые показатели эффективности работы отдела маркетинга, настраивать их отслеживание, контроль и прогнозирование.\n\n<b>Подавай заявку:</b>\n$url",
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "хуй" ) !== false
		|| stripos( $this->result["message"]["text"], "иди на хуй" ) !== false 
		|| stripos( $this->result["message"]["text"], "Хуй" ) !== false 
		|| stripos( $this->result["message"]["text"], "пизд" ) !== false
		|| stripos( $this->result["message"]["text"], "издец" ) !== false 
		|| stripos( $this->result["message"]["text"], "блядь" ) !== false 
		|| stripos( $this->result["message"]["text"], "блять" ) !== false 
		|| stripos( $this->result["message"]["text"], "бля" ) !== false
		|| stripos( $this->result["message"]["text"], "пидор" ) !== false 
		|| stripos( $this->result["message"]["text"], "дам в рот" ) !== false 
		|| stripos( $this->result["message"]["text"], "пидр" ) !== false 
		|| stripos( $this->result["message"]["text"], "хуесос" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебать" ) !== false 
		|| stripos( $this->result["message"]["text"], "ебал" ) !== false 
		|| stripos( $this->result["message"]["text"], "отсоси" ) !== false 
		|| stripos( $this->result["message"]["text"], "пососи" ) !== false 
		|| stripos( $this->result["message"]["text"], "выеб" ) !== false 
		|| stripos( $this->result["message"]["text"], "за щёку" ) !== false 
		|| stripos( $this->result["message"]["text"], "за щеку" ) !== false 
		|| stripos( $this->result["message"]["text"], "миньет" ) !== false 
		|| stripos( $this->result["message"]["text"], "сука" ) !== false 
		|| stripos( $this->result["message"]["text"], "сучка" ) !== false 
		|| stripos( $this->result["message"]["text"], "Сучка" ) !== false 
		|| stripos( $this->result["message"]["text"], "долбой" ) !== false 
		|| stripos( $this->result["message"]["text"], "долбоё" ) !== false 	
		|| stripos( $this->result["message"]["text"], "ахуй" ) !== false 	
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			'text' => "\n\nУ нас матом <b>не ругаются</b>, никого <b>не оскорбляют</b>, <b>не провоцируют</b>.\n\nПожалуйста, придерживаясь этих <b>простых правил</b>, нажми /start и повтори вопрос ещё раз.\n\nВ противном случае я тебя забаню и выебу.\nШучу - не забаню."
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "очкобет" ) !== false
		|| stripos( $this->result["message"]["text"], "очко бет" ) !== false 
		|| stripos( $this->result["message"]["text"], "ochko bet" ) !== false 
		|| stripos( $this->result["message"]["text"], "Чабел" ) !== false 
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://spalah.com/course/e-mail-marketing#formatsHighlight",
			'text' => "\n\nПо всем вопросам обращайся к CEO и Founder <b>Очкобет Inc</b> - дону Витальяно.\n\nБесплатный совет: проси с уважением."
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "аркет" ) !== false 
		|| stripos( $this->result["message"]["text"], "arket" ) !== false
		|| stripos( $this->result["message"]["text"], "урс по рекламе" ) !== false
		|| stripos( $this->result["message"]["text"], "аркетинг по рекламе" ) !== false
		){
			$this->api->sendMessage([
			'parse_mode' => 'HTML', 
			'disable_web_page_preview' => false, 
			'text' => $text,
			$url = "https://esputnik.com",
			$url1 = "https://netpeak.ua",
			$url2 = "https://owox.ua",
			'text' => "<em>Ты это искал, верно?\nЕсли нет, переформулируй вопрос и попробуй ещё!</em>\n\nm a r k e t i n g\n\nВ создании курсов направления принимали участие сотрудники компаний <b>eSputnik</b>, <b>Netpeak</b>, <b>Owox</b> \n\n$url\n\n$url1\n\n$url2\n\n<b>Выбери курс:</b>",
			'reply_markup' => json_encode( [
				'inline_keyboard'=> $this->keyboards['marketing']
				] )
			]);
		}

		elseif( stripos( $this->result["message"]["text"], "обрый день" ) !== false
		|| stripos( $this->result["message"]["text"], "оброе утро" ) !== false 
		|| stripos( $this->result["message"]["text"], "Привет тебе" ) !== false 
		|| stripos( $this->result["message"]["text"], "риветик" ) !== false 
		|| stripos( $this->result["message"]["text"], "privet" ) !== false 
		|| stripos( $this->result["message"]["text"], "Хай" ) !== false 
		|| stripos( $this->result["message"]["text"], "хай" ) !== false 
		|| stripos( $this->result["message"]["text"], "hi" ) !== false 
		|| stripos( $this->result["message"]["text"], "hay" ) !== false 
		|| stripos( $this->result["message"]["text"], "доброго" ) !== false 
		|| stripos( $this->result["message"]["text"], "оброй ночи" ) !== false 
		|| stripos( $this->result["message"]["text"], "здарова" ) !== false
		|| stripos( $this->result["message"]["text"], "здорово" ) !== false
		|| stripos( $this->result["message"]["text"], "даров" ) !== false
		|| stripos( $this->result["message"]["text"], "прувет" ) !== false
		|| stripos( $this->result["message"]["text"], "pryvet" ) !== false
		|| stripos( $this->result["message"]["text"], "good day" ) !== false
		|| stripos( $this->result["message"]["text"], "hello" ) !== false
		|| stripos( $this->result["message"]["text"], "ень добрый" ) !== false
		|| stripos( $this->result["message"]["text"], "ривет" ) !== false 
		|| stripos( $this->result["message"]["text"], "ратути" ) !== false 
		){
			$this->api->sendMessage( "Привет, @" . $this->result["message"]["from"]["username"] . "!\n\nНапиши свой вопрос, очень постараюсь ответить :) " );
			}

		else{
			$this->api->sendMessage([
				'parse_mode' => 'HTML', 
				'disable_web_page_preview' => false, 
				'text' => $text,
				'text' => "Сорри! Не совсем понял!\n\nПереформулируй вопрос, пожалуйста, и попробуй ещё :)\n\n\n<em>Если ты слишком часто видишь это сообщение, вероятно, тебе лучше пообщаться с человеком.</em>\n\n/human <em>осведомлённее меня в некоторых вопросах (но это не точно).</em>"
			]);
		}
	}

	/**
	 * Обработка ввода команды "Инлайн" отправляет сообщение с клавиатурой, прикрепелнной к сообщению.
	 */
	function cmd_free(){
		$this->api->sendMessage([
			'text'=>"Ниже выведены кнопки, нажатие на которые может выполнять какие-то действия. Бот не ответит на кнопке будет отображаться иконка часиков.",
			'reply_markup' => json_encode( [
				'inline_keyboard'=> $this->keyboards['inline']
			] )
		]);
	}


	
	// Ответ на нажатие кнопки с обработкой дополнительных параметров
	function callback_act45( $query ){
		$text = "Вы нажали на кнопку \"C параметрами\" ";
		$text .= "Вот какие параметры были переданы с нажанием кнопки:\n {$query}";
		$this->callbackAnswer( $text, $this->keyboards['back'] );
	}

	// Ответ на нажатие кнопки всплывающим окном
	function callback_act35( $query ){
		$this->api->answerCallbackQuery( [
			'callback_query_id' => $this->result['callback_query']["id"],
			'text' => "Вы нажали кнопку \"Действие 3\"",
			'show_alert' => true
		] );
	}	

	// Ответ на кнопку "Закрыть"
	function callback_logout(){
		$this->api->answerCallbackQuery( $this->result['callback_query']["id"] );
		$this->api->deleteMessage( $this->result['callback_query']['message']['message_id'] );
	}

}

?>
