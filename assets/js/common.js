jQuery( function() {
	jQuery( '.popup-with-zoom-anim' ).magnificPopup( {
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 1000,
	} );

	jQuery( 'a[href=\\#subscribe]' ).click( function() {
		jQuery( '#subscribe .formname' ).val( jQuery( this ).data( 'form' ) );
	} );

	jQuery( '.subscribe' ).submit( function() { //Change
		const th = jQuery( this );

		jQuery( '.success' ).addClass( 'visible' );
		setTimeout( function() {
			th.trigger( 'reset' );
			jQuery( '.success' ).removeClass( 'visible' );
			jQuery.magnificPopup.close();
		}, 4100 );
		return false;
	} );

	//Chrome Smooth Scroll
	try {
		jQuery.browserSelector();
		if ( jQuery( 'html' ).hasClass( 'chrome' ) ) {
			jQuery.smoothScroll();
		}
	} catch ( err ) {

	}

	jQuery( 'img, a' ).on( 'dragstart', function( event ) {
		event.preventDefault();
	} );

	jQuery( '.header-phones-button' ).click( function() {
		jQuery( '.menu-phone' ).fadeIn( 'fast' );
	} );

	jQuery( document ).mouseup( function( e ) {
		const div = jQuery( '.menu-phone' );
		if ( ! div.is( e.target ) &&
		    div.has( e.target ).length === 0 ) {
			div.fadeOut( 'fast' );
		}
	} );

	jQuery( '.contacts-mobile' ).click( function() {
		jQuery( '.big-menu-mobile' ).fadeIn( '' );
	} );

	jQuery( '.big-menu-mobile-close' ).click( function() {
		jQuery( '.big-menu-mobile' ).fadeOut( '' );
	} );
} );
