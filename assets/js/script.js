//Menu
function burgerMenu() {
    let menu = jQuery('.burger-menu');
    let button = jQuery('.burger-menu_button');
    let overlay = jQuery('.burger-menu_overlay');

    button.on('click', (e) => {
        e.preventDefault();
        toggleMenu(menu);
    });

    overlay.on('click', () => toggleMenu(menu));
}


function toggleMenu(menu) {
    menu.toggleClass('burger-menu_active');

    if (menu.hasClass('burger-menu_active')) {
        jQuery('body').css('overlow', 'hidden');
    } else {
        jQuery('body').css('overlow', 'visible');
    }
}

jQuery(document).ready(function () {

    burgerMenu();

    jQuery('.burger-menu_link').on('click', function (e){
        let menu = jQuery('.burger-menu');
        let href = jQuery(this).data('link');


        jQuery('html, body').animate({
            scrollTop: jQuery(href).offset().top - 130
        }, 200);

        toggleMenu(menu);
    });
});