jQuery( document ).ready( function() {
	jQuery( '.portfolio_item' ).each( function( i ) {
		jQuery( this ).find( 'a' ).attr( 'href', '#work_' + i );
		jQuery( this ).find( '.podrt_descr' ).attr( 'id', 'work_' + i );
	} );

	jQuery( '#portfolio_grid' ).mixItUp();

	jQuery( '.s_portfolio li' ).click( function() {
		jQuery( '.s_portfolio li' ).removeClass( 'active' );
		jQuery( this ).addClass( 'active' );
	} );

	jQuery( '.popup' ).magnificPopup( { type: 'image' } );
	jQuery( '.popup_content' ).magnificPopup( {
		type: 'inline',
		midClick: true,
	} );

	// jQuery( '.section_header' ).animated( 'fadeInUp', 'fadeOutDown' );
	//
	// jQuery( '.animation_1' ).animated( 'flipInY', 'fadeOutDown' );
	// jQuery( '.animation_2' ).animated( 'fadeInLeft', 'fadeOutDown' );
	// jQuery( '.animation_3' ).animated( 'fadeInRight', 'fadeOutDown' );
	//
	// jQuery( '.left .resume_item' ).animated( 'fadeInLeft', 'fadeOutDown' );
	// jQuery( '.right .resume_item' ).animated( 'fadeInRight', 'fadeOutDown' );

	function heightDetect() {
		jQuery( '.main_head' ).css( 'height', jQuery( window ).height() );
	}
	heightDetect();
	jQuery( window ).resize( function() {
		heightDetect();
	} );

	jQuery( '.toggle_mnu' ).click( function() {
		jQuery( '.sandwich' ).toggleClass( 'active' );
	} );

	jQuery( '.top_mnu ul a' ).click( function() {
		jQuery( '.top_mnu' ).fadeOut( 600 );
		jQuery( '.sandwich' ).toggleClass( 'active' );
		jQuery( '.top_text' ).css( 'opacity', '1' );
	} ).append( '<span>' );

	jQuery( '.toggle_mnu' ).click( function() {
		if ( jQuery( '.top_mnu' ).is( ':visible' ) ) {
			jQuery( '.top_text' ).css( 'opacity', '1' );
			jQuery( '.top_mnu' ).fadeOut( 600 );
			jQuery( '.top_mnu li a' ).removeClass( 'fadeInUp animated' );
		} else {
			jQuery( '.top_text' ).css( 'opacity', '.1' );
			jQuery( '.top_mnu' ).fadeIn( 600 );
			jQuery( '.top_mnu li a' ).addClass( 'fadeInUp animated' );
		}
	} );

	// jQuery( 'input, select, textarea' ).jqBootstrapValidation();

	// jQuery( '.top_mnu ul a' ).mPageScroll2id();
} );
jQuery( window ).load( function() {
	jQuery( '.loader_inner' ).fadeOut();
	jQuery( '.loader' ).delay( 400 ).fadeOut( 'slow' );

	// jQuery( '.top_text h1' ).animated( 'fadeInDown', 'fadeOutUp' );
	// jQuery( '.top_text p' ).animated( 'fadeInUp', 'fadeOutDown' );
} );
