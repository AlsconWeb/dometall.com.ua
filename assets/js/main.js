( function( $ ) {

	skel.breakpoints( {
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		largez: '(max-width: 1081px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)'
	} );

	$( function() {

		var $window = $( window ),
			$body = $( 'body' );

		// Touch mode.
		if ( skel.vars.mobile )
			$body.addClass( 'is-touch' );

		// Fix: Placeholder polyfill.
		$( 'form' ).placeholder();

		// Prioritize "important" elements on medium.
		skel.on( '+medium -medium', function() {
			$.prioritize(
				'.important\\28 medium\\29',
				skel.breakpoint( 'medium' ).active
			);
		} );

		// Scrolly links.
		$( '.scrolly' ).scrolly( {
			speed: 2000
		} );


		// Parallax.
		// Disabled on IE (choppy scrolling) and mobile platforms (poor performance).
		if ( skel.vars.browser == 'ie'
			|| skel.vars.mobile ) {

			$.fn._parallax = function() {

				return $( this );

			};

		} else {

			$.fn._parallax = function() {

				$( this ).each( function() {

					var $this = $( this ),
						on, off;

					on = function() {

						$this
							.css( 'background-position', 'center 0px' );

						$window
							.on( 'scroll._parallax', function() {

								var pos = parseInt( $window.scrollTop() ) - parseInt( $this.position().top );

								$this.css( 'background-position', 'center ' + ( pos * -0.15 ) + 'px' );

							} );

					};

					off = function() {

						$this
							.css( 'background-position', '' );

						$window
							.off( 'scroll._parallax' );

					};

					skel.on( 'change', function() {

						if ( skel.breakpoint( 'medium' ).active )
							( off )();
						else
							( on )();

					} );

				} );

				return $( this );

			};

			$window
				.on( 'load resize', function() {
					$window.trigger( 'scroll' );
				} );

		}

		// Spotlights.
		var $spotlights = $( '.spotlight' );

		$spotlights
			._parallax()
			.each( function() {

				var $this = $( this ),
					on, off;

				on = function() {

					// Use main <img>'s src as this spotlight's background.
					$this.css( 'background-image', 'url("' + $this.find( '.image.main > img' ).attr( 'src' ) + '")' );

					// Enable transitions (if supported).
					if ( skel.canUse( 'transition' ) ) {

						var top, bottom, mode;

						// Side-specific scrollex tweaks.
						if ( $this.hasClass( 'top' ) ) {

							mode = 'top';
							top = '-20%';
							bottom = 0;

						} else {

							mode = 'middle';
							top = 0;
							bottom = 0;

						}

						// Add scrollex.
						$this.scrollex( {
							mode: mode,
							top: top,
							bottom: bottom,
							initialize: function( t ) {
								$this.addClass( 'inactive' );
							},
							terminate: function( t ) {
								$this.removeClass( 'inactive' );
							},
							enter: function( t ) {
								$this.removeClass( 'inactive' );
							},

							// Uncomment the line below to "rewind" when this spotlight scrolls out of view.

							//leave:	function(t) { $this.addClass('inactive'); },

						} );

					}

				};

				off = function() {

					// Clear spotlight's background.
					$this.css( 'background-image', '' );

					// Disable transitions (if supported).
					if ( skel.canUse( 'transition' ) ) {

						// Remove scrollex.
						$this.unscrollex();

					}

				};

				skel.on( 'change', function() {

					if ( skel.breakpoint( 'medium' ).active )
						( off )();
					else
						( on )();

				} );

			} );

		// Wrappers.
		var $wrappers = $( '.wrapper' );

		$wrappers
			.each( function() {

				var $this = $( this ),
					on, off;

				on = function() {

					if ( skel.canUse( 'transition' ) ) {

						$this.scrollex( {
							top: 250,
							bottom: 0,
							initialize: function( t ) {
								$this.addClass( 'inactive' );
							},
							terminate: function( t ) {
								$this.removeClass( 'inactive' );
							},
							enter: function( t ) {
								$this.removeClass( 'inactive' );
							},

							// Uncomment the line below to "rewind" when this wrapper scrolls out of view.

							//leave:	function(t) { $this.addClass('inactive'); },

						} );

					}

				};

				off = function() {

					if ( skel.canUse( 'transition' ) )
						$this.unscrollex();

				};

				skel.on( 'change', function() {

					if ( skel.breakpoint( 'medium' ).active )
						( off )();
					else
						( on )();

				} );

			} );

		// Banner.
		var $banner = $( '#banner' );

		$banner
			._parallax();

	} );

	// Tiles.
	var $tiles = $( '.tiles > article' );

	$tiles.each( function() {

		var $this = $( this ),
			$image = $this.find( '.image' ), $img = $image.find( 'img' ),
			$link = $this.find( '.link' ),
			x;

		// Image.

		// Set image.
		$this.css( 'background-image', 'url(' + $img.attr( 'src' ) + ')' );

		// Set position.
		if ( x = $img.data( 'position' ) )
			$image.css( 'background-position', x );

		// Hide original.
		$image.hide();

		// Link.
		if ( $link.length > 0 ) {

			$x = $link.clone()
				.text( '' )
				.addClass( 'primary' )
				.appendTo( $this );

			$link = $link.add( $x );

			$link.on( 'click', function( event ) {

				var href = $link.attr( 'href' );

				// Prevent default.
				event.stopPropagation();
				event.preventDefault();

				// Target blank?
				if ( $link.attr( 'target' ) == '_blank' ) {

					// Open in new tab.
					window.open( href );

				}

				// Otherwise ...
				else {

					// Start transitioning.
					$this.addClass( 'is-transitioning' );
					$wrapper.addClass( 'is-transitioning' );

					// Redirect.
					window.setTimeout( function() {
						location.href = href;
					}, 500 );

				}

			} );

		}

	} );

} )( jQuery );

jQuery( document ).ready( function( $ ) {
	$( '.partners' ).slick( {
		slidesToShow: 5,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					slidesToShow: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					slidesToShow: 1
				}
			}
		]
	} );

	jQuery( '.wpcf7-tel' ).mask( '+38(999)999-99-99' );
} );