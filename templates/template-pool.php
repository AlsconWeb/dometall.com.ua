<?php
/**
 * Template Name: Pool page
 */

get_header('pool');
?>
    <div class="section" id="section0">
        <div class="text-slide">
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <!--img src="https://img.dometall.com.ua/pool_1.png" alt=""-->
                    </div>
                    <div class="col-md-6">
                        <p><?php echo carbon_get_the_post_meta( 'crb_screen_first' ) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="section1">
        <div class="text-slide">
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <p><?php echo carbon_get_the_post_meta( 'crb_screen_second' ) ?></p>

                    </div>
                    <div class="col-md-6">
                        <!--img src="https://img.dometall.com.ua/pool_2.png" alt=""-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="section2">
        <section id="four" class="wrapper style1 special fade-up">
            <div class="container">
                <header class="major">
                    <h2><?php echo carbon_get_the_post_meta( 'crb_screen_third' ) ?></h2>
                </header>
                <div class="box alt">
                    <div class="rows uniform">
                        <section class="2u 6u(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-fingerprint"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_first' ) ?></p>
                            </div>
                        </section>
                        <section class="2u 6u$(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-shoe-prints"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_second' ) ?></p>
                            </div>
                        </section>
                        <section class="2u 6u(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-ruler"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_third' ) ?></p>
                            </div>
                        </section>
                        <section class="2u 6u(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-swimmer"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_fourth' ) ?></p>
                            </div>
                        </section>
                        <section class="2u 6u(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-trophy"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_fifth' ) ?></p>
                            </div>
                        </section>
                        <section class="2u 6u(medium) 12u(xsmall)">
                            <span class="icon alt major fas fa-swimming-pool"></span>
                            <div class="main_text">
                                <p><?php echo carbon_get_the_post_meta( 'crb_text_six' ) ?></p>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div class="section" id="section">
        <h1 style="color: #4ABA4A;">Наши цены вас удивят!</h1>
        <p style="margin-top: 1em; line-height: 1.2em;">
            Сделайте заказ и получите скидку 10% на лестницу в бассейн!
        </p>
    </div>


    <div class="section" id="section2">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2566.815782266068!2d36.30030221571358!3d49.95855807941056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41270a58f194ee87%3A0xc94e9bb515ad3be0!2z0L_RgNC-0YHQvy4g0JvRjNCy0LAg0JvQsNC90LTQsNGDLCAxNTUsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2MTAwMA!5e0!3m2!1sru!2sua!4v1581609910815!5m2!1sru!2sua"
                width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
<?php
get_footer('pool');
