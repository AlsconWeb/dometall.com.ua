<?php
/**
 * Template Name: Home page new
 */

get_header( 'new' );
?>

	<main id="main">
		<section id="first-screen" class="first-screen">
			<div class="top">
				<div class="container">
					<div class="content">
						<h1><?php echo esc_html( carbon_get_the_post_meta( 'crb_title' ) ) ?></h1>
						<ul>
							<?php
							$list = carbon_get_the_post_meta( 'crb_lists' );
							foreach ( $list as $item ) {
								echo '<li>&nbsp&nbsp' . esc_html( $item['text'] ) . '</li>';
							}
							?>
						</ul>

						<a href="#fifth-screen" class="btn">Останні роботи</a>
						<a href="#contact-modal"
						   class="button special btn"
						   rel="modal:open"><?php esc_html_e( 'Замовити консультацію', 'dometall' ); ?></a>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					<div class="column">
						<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_title_first' ) ); ?></h4>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_first' ) ); ?></p>
					</div>
					<div class="column">
						<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_title_second' ) ); ?></h4>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_second' ) ); ?></p>
					</div>
					<div class="column">
						<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_title_third' ) ); ?></h4>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_third' ) ); ?></p>
					</div>
					<div class="column">
						<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_title_fourth' ) ); ?></h4>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_fourth' ) ); ?></p>
					</div>
				</div>
			</div>
		</section><!-- #first-screen -->

		<section id="second-screen" class="second-screen">
			<div class="container">
				<h3>ПЕРЕВАГИ РОБОТИ З НАМИ</h3>

				<div class="icons-list">
					<div class="icon-column">
						<div class="icon-box">
						</div>
						<p>Індивідуальна система знижок для кожного клієнта</p>
					</div>

					<div class="icon-column">
						<div class="icon-box">
						</div>
						<p>Максимально швидкі терміни виготовлення сходів</p>
					</div>

					<div class="icon-column">
						<div class="icon-box">
						</div>
						<p>Можливість безготівкової форми оплати</p>
					</div>

					<div class="icon-column">
						<div class="icon-box">
						</div>
						<p>12 місяців гарантії на вироби з металоконструкцій</p>
					</div>
				</div>
			</div>
		</section><!-- #second-screen -->

		<section id="third-screen" class="third-screen">
			<div class="container">
				<h3>ПОПУЛЯРНІ КАТЕГОРІЇ ПОСЛУГ</h3>
			</div>
			<div class="container-full">
				<div class="services-list">
					<a href="<?php echo esc_url( get_permalink( carbon_get_the_post_meta( 'crb_s_link_first' )[0]['id'] ) ); ?>"
					   style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/perila.jpg' ?>)">
						<div class="content">
							<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_services_first' ) ); ?></h4>
							<span class="btn"><?php echo esc_html( carbon_get_the_post_meta( 'crb_s_l_text_first' ) ); ?></span>
						</div>
					</a>
					<a href="<?php echo esc_url( get_permalink( carbon_get_the_post_meta( 'crb_s_link_second' )[0]['id'] ) ); ?>"
					   style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/vnutrennie.jpg' ?>)">
						<div class="content">
							<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_services_second' ) ); ?></h4>
							<span class="btn"><?php echo esc_html( carbon_get_the_post_meta( 'crb_s_l_text_second' ) ); ?></span>
						</div>
					</a>
					<a href="<?php echo esc_url( get_permalink( carbon_get_the_post_meta( 'crb_s_link_third' )[0]['id'] ) ); ?>"
					   style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/vneshnie.png' ?>)">
						<div class="content">
							<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_services_third' ) ); ?></h4>
							<span class="btn"><?php echo esc_html( carbon_get_the_post_meta( 'crb_s_l_text_third' ) ); ?></span>
						</div>
					</a>
					<a href="<?php echo esc_url( get_permalink( carbon_get_the_post_meta( 'crb_s_link_fourth' )[0]['id'] ) ); ?>"
					   style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/metalokonstrukcii.jpg' ?>)">
						<div class="content">
							<h4><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_services_fourth' ) ); ?></h4>
							<span class="btn"><?php echo esc_html( carbon_get_the_post_meta( 'crb_s_l_text_fourth' ) ); ?></span>
						</div>
					</a>
				</div>
			</div>
			<div class="container">
				<a href="#contact-modal" rel="modal:open" class="btn">Замовити консультацію</a>
			</div>
		</section><!-- #third-screen -->

		<section id="fourth-screen" class="fourth-screen">
			<div class="container">
				<h3><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_title' ) ); ?></h3>
				<div class="benefits-list">
					<div class="benefit-item">
						<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_first' ) ); ?></h5>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_first_t' ) ); ?></p>
					</div>
					<div class="benefit-item">
						<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_second' ) ); ?></h5>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_second_t' ) ); ?></p>
					</div>
					<div class="benefit-item">
						<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_third' ) ); ?></h5>
						<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_f_third_t' ) ); ?></p>
					</div>
				</div>
			</div>
		</section><!-- #fourth-screen -->

		<section id="fifth-screen" class="fifth-screen">
			<div class="container">
				<h3>НАШІ ОСТАННІ РОБОТИ</h3>

				<div class="portfolio-list">
					<?php
					$gallery = carbon_get_the_post_meta( 'crb_gallery' );
					foreach ( $gallery as $item ) { ?>
						<div class="portfolio-item" style="background-image: url(<?php echo $item['img'] ?>)">
							<a href="#" class="popup_content"></a>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="<?php echo esc_url( $item['img'] ); ?>" alt="Alt">
									</div>
								</div>
							</div>
						</div>
						<?php
					} ?>
				</div>

				<a href="/our-works" class="btn">Дивитись усі роботи</a>
			</div>
		</section>

		<section id="partners">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="partners">
							<?php
							$partners_logo = carbon_get_the_post_meta( 'crb_partners' );
							if ( ! empty( $partners_logo ) ):
								foreach ( $partners_logo as $logo ):
									?>
									<div class="item">
										<a href="<?php echo ! empty( $logo['crb_link'] ) ? esc_url( $logo['crb_link'] ) : '#' ?>">
											<img src="<?php echo esc_url( $logo['logo'] ); ?>" alt="partner logo">
										</a>
									</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main><!-- #main -->

<?php
get_footer( 'new' );
