<?php
/**
 * Template Name: Feedback page
 */

get_header('new');
?>

    <section id="one" class="style1 bottom text-content new-style">

        <div class="container">
            <?php
            while (have_posts()) :
                the_post();

                the_content();

            endwhile; // End of the loop.
            ?>
        </div>

    </section>


<?php
//get_template_part( 'template-parts/content', 'contact' );

get_footer('new');
