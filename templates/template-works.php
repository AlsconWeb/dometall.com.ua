<?php
/**
 * Template Name: Our works page
 */

get_header( 'new' );
?>
	<section id="portfolio" class="s_portfolio">
		<div class="section_content">
			<div class="container">
				<div class="row">
					<div class="filter_div controls">
						<ul>
							<li class="filter active" data-filter="all">ВСІ РОБОТИ</li>
							<?php
							$gallery = carbon_get_the_post_meta( 'crb_cat_gallery' );
							$i       = 1;

							foreach ( $gallery as $item ) {
								echo '<li class="filter" data-filter=".category-' . $i . '">' . $item['text'] . '</li>';

								$i ++;
							}
							?>
						</ul>
					</div>


					<div id="portfolio_grid">

						<?php
						$gallery = carbon_get_the_post_meta( 'crb_cat_gallery' );
						$i       = 1;

						foreach ( $gallery as $item ) {
							foreach ( $item['crb_media_gallery'] as $single ) {
								?>
								<div class="mix col-md-3 col-sm-6 col-xs-12 portfolio_item category-<?php echo $i ?>">
									<?php echo wp_get_attachment_image( $single, 'medium' ); ?>
									<div class="port_item_cont">
										<a href="#" class="popup_content"><img
													src="<?php echo get_template_directory_uri() . '/assets/img/lupa.png' ?>"></a>
									</div>
									<div class="hidden">
										<div class="podrt_descr">
											<div class="modal-box-content">
												<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
												<?php echo wp_get_attachment_image( $single, 'full' ); ?>
											</div>
										</div>
									</div>
								</div>
								<?php
							}
							$i ++;
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'new' );
