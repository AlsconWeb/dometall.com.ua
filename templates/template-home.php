<?php
/**
 * Template Name: Home page
 */

get_header();
?>


	<div class="mobile-design">
		<div class="mob-first-block">
			<h1><?php echo carbon_get_the_post_meta( 'crb_title' ) ?></h1>
		</div>
	</div>
	<section id="four_head" class="wrapper style1 special2 fade-up">
		<header class="main-head">
			<div class="container">
				<div class="row">
					<div class="header-content clearfix">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h1 style="color: #000;"><?php echo carbon_get_the_post_meta( 'crb_title' ) ?></h1>
							<ul>
								<?php
								$list = carbon_get_the_post_meta( 'crb_lists' );
								foreach ( $list as $item ) {
									echo '<li><i class="fa fa-check"></i>&nbsp&nbsp' . $item['text'] . '</li>';
								}
								?>
							</ul>
							<a href="#portfolio" class="button special">Останні роботи</a>
						</div>

					</div>
				</div>
			</div>
			<section class="s-dark">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="s-dark-item">
								<h4><?php echo carbon_get_the_post_meta( 'crb_title_first' ) ?></h4>
								<p><?php echo carbon_get_the_post_meta( 'crb_text_first' ) ?></p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="s-dark-item">
								<h4><?php echo carbon_get_the_post_meta( 'crb_title_second' ) ?></h4>
								<p><?php echo carbon_get_the_post_meta( 'crb_text_second' ) ?></p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="s-dark-item">
								<h4><?php echo carbon_get_the_post_meta( 'crb_title_third' ) ?></h4>
								<p><?php echo carbon_get_the_post_meta( 'crb_text_third' ) ?></p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="s-dark-item">
								<h4><?php echo carbon_get_the_post_meta( 'crb_title_fourth' ) ?></h4>
								<p><?php echo carbon_get_the_post_meta( 'crb_text_fourth' ) ?></p>
							</div>

						</div>
					</div>
				</div>
			</section>
		</header>
	</section>

	<section id="portfolio" class="spotlight style1 bottom s_portfolio">

		<div class="content">
			<div class="container">
				<div class="rows">
					<h2>НАШІ ОСТАННІ РОБОТИ</h2>
					<div id="portfolio_grid">
						<?php
						$gallery = carbon_get_the_post_meta( 'crb_gallery' );
						foreach ( $gallery as $item ) { ?>
							<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2 category-3">

								<img src="<?php echo $item['img'] ?>" class="prev" alt="Alt">
								<div class="port_item_cont">
									<a href="#" class="popup_content">
										<img src="<?php echo get_template_directory_uri() . '/assets/img/lupa.png' ?>">
									</a>
								</div>
								<div class="hidden">
									<div class="podrt_descr">
										<div class="modal-box-content">
											<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
											<img src="<?php echo $item['img'] ?>" alt="Alt">
										</div>
									</div>
								</div>
							</div>
							<?php
						} ?>
					</div>
					<h2>Дом Металла - команда профессионалов</h2>
					<div class="4u 12u$(medium)">
						<p><?php echo carbon_get_the_post_meta( 'crb_column_first' ) ?></p>
					</div>
					<div class="4u 12u$(medium)">
						<p><?php echo carbon_get_the_post_meta( 'crb_column_second' ) ?></p>

					</div>
					<div class="4u$ 12u$(medium)">
						<p><?php echo carbon_get_the_post_meta( 'crb_column_third' ) ?></p>
					</div>

				</div>
			</div>
		</div>
	</section>
	<div class="tiles">
		<div class="grid left-block">
			<figure class="effect-apollo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/perila.jpg' ?>"
					 alt="Поручни для бассейна фото"
					 title="Поручни для бассейна"/>
				<figcaption>
					<h2><?php echo carbon_get_the_post_meta( 'crb_services_first' ) ?></h2>
					<p><?php echo carbon_get_the_post_meta( 'crb_s_l_text_first' ) ?></p>
					<a href="<?php echo get_permalink( carbon_get_the_post_meta( 'crb_s_link_first' )[0]['id'] ); ?>">Подробнее</a>
				</figcaption>
			</figure>
		</div>
		<div class="grid right-block big">
			<figure class="effect-apollo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/vnutrennie.jpg' ?>"
					 alt="Внутренние лестницы фото"
					 title="Поручни для бассейна"/>
				<figcaption>
					<h2><?php echo carbon_get_the_post_meta( 'crb_services_second' ) ?></h2>
					<p><?php echo carbon_get_the_post_meta( 'crb_s_l_text_second' ) ?></p>
					<a href="<?php echo get_permalink( carbon_get_the_post_meta( 'crb_s_link_second' )[0]['id'] ); ?>">Подробнее</a>
				</figcaption>
			</figure>
		</div>

		<div class="grid right-block big">
			<figure class="effect-apollo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/vneshnie.png' ?>"
					 alt="Внутренние лестницы фото"
					 title="Поручни для бассейна"/>
				<figcaption>
					<h2><?php echo carbon_get_the_post_meta( 'crb_services_third' ) ?></h2>
					<p><?php echo carbon_get_the_post_meta( 'crb_s_l_text_third' ) ?></p>
					<a href="<?php echo get_permalink( carbon_get_the_post_meta( 'crb_s_link_third' )[0]['id'] ); ?>">Подробнее</a>
				</figcaption>
			</figure>
		</div>

		<div class="grid left-block">
			<figure class="effect-apollo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/metalokonstrukcii.jpg' ?>"
					 alt="Поручни для бассейна фото"
					 title="Поручни для бассейна"/>
				<figcaption>
					<h2><?php echo carbon_get_the_post_meta( 'crb_services_fourth' ) ?></h2>
					<p><?php echo carbon_get_the_post_meta( 'crb_s_l_text_fourth' ) ?></p>
					<a href="<?php echo get_permalink( carbon_get_the_post_meta( 'crb_s_link_fourth' )[0]['id'] ); ?>">Подробнее</a>
				</figcaption>
			</figure>
		</div>
	</div>

	<section id="four" class="wrapper style1 special fade-up">
		<div class="container">
			<header class="major">
				<h2><?php echo carbon_get_the_post_meta( 'crb_benefits_title' ) ?></h2>
			</header>
			<div class="box alt">
				<div class="rows uniform">
					<section class="3u 6u$(medium) 12u$(xsmall)">
						<span><img src="<?php echo get_template_directory_uri() . '/assets/img/preim1.png' ?>"></span>
						<div class="main_text">
							<p><?php echo carbon_get_the_post_meta( 'crb_benefit_first' ) ?></p>
						</div>
					</section>
					<section class="3u 6u$(medium) 12u$(xsmall)">
						<span><img src="<?php echo get_template_directory_uri() . '/assets/img/preim2.png' ?>"></span>
						<div class="main_text">
							<p><?php echo carbon_get_the_post_meta( 'crb_benefit_second' ) ?></p>
						</div>
					</section>
					<section class="3u 6u$(medium) 12u$(xsmall)">
						<span><img src="<?php echo get_template_directory_uri() . '/assets/img/preim3.png' ?>"></span>
						<div class="main_text">
							<p><?php echo carbon_get_the_post_meta( 'crb_benefit_third' ) ?></p>
						</div>
					</section>
					<section class="3u$ 6u(medium) 12u$(xsmall)">
						<span><img src="<?php echo get_template_directory_uri() . '/assets/img/preim4.png' ?>"></span>
						<div class="main_text">
							<p><?php echo carbon_get_the_post_meta( 'crb_benefit_fourth' ) ?></p>
						</div>
					</section>
				</div>
			</div>

			<div class="hidden_text">
				<p>Замовте швидку консультацію та отримайте знижку 10%<br> на всі види металоконструкцій</p>
			</div>
			<p><a href="#subscribe" class="button special popup-with-zoom-anim"
				  data-form="Мобильная форма - Преимущества - скидка 10%">Замовити консультацію</a></p>
		</div>
	</section>

<?php
get_template_part( 'template-parts/content', 'contact' );

get_footer();
