<?php
/**
 * Template Name: About page
 */

get_header('new');
?>

    <section id="one" class="about-page-section">
        <div class="container">
            <?php
            while (have_posts()) :
                the_post();

                the_content();

            endwhile; // End of the loop.
            ?>
        </div>
    </section><!-- #one -->

<?php
//get_template_part( 'template-parts/content', 'contact' );

get_footer('new');
