<?php
/**
 * Template Name: Success page
 */

get_header( 'new' );
?>
	<div id="fullpage" class="fullpage-wrapper" style="height: 100%; position: relative; touch-action: none; top: 0px;">
		<div class="section fp-section active fp-table fp-completely" id="section0" data-fp-styles="null"
			 style="height: 637px; background-color: rgb(28, 29, 38);">
			<div class="fp-tableCell">
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="main_title">
								<h1>Спасибо!</h1>
							</div>
						</div>
					</div>
				</div>

				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="first_link">
								<div class="success-text">Ваша заявка прийнята. Ми зв'яжемося з Вами найближчим часом.
								</div>
								<div class="home-button"><a href="https://dometall.com.ua">На главную</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer( 'new' );
