<?php
/**
 * Template Name: Rails page
 */

get_header( 'new' );
?>

	<section id="first-screen" class="first-screen">
		<div class="top">
			<div class="container">
				<div class="content">
					<h1><?php echo carbon_get_the_post_meta( 'crb_title' ) ?></h1>
					<ul>
						<?php
						$list = carbon_get_the_post_meta( 'crb_lists' );
						foreach ( $list as $item ) {
							echo '<li>&nbsp&nbsp' . $item['text'] . '</li>';
						}
						?>
					</ul>

					<a href="#contact-modal" rel="modal:open" class="btn">Замовити</a>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="container">
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_first' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_first' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_second' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_second' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_third' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_third' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_fourth' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_fourth' ) ?></p>
				</div>
			</div>
		</div>
	</section><!-- #first-screen -->

	<section class="second-rails-section spotlight">
		<div class="container">
			<div class="row">
				<div class="type-raills">

					<h3> <?php echo carbon_get_the_post_meta( 'title11' ); ?></h3>
					<?php
					$types = carbon_get_the_post_meta( 'crb_types' );
					foreach ( $types as $item ) { ?>
						<div class="col-md-6">
							<?php echo wp_get_attachment_image( $item['img'], 'full' ); ?>
							<p><?php echo $item['text'] ?></p>
						</div>
						<?php
					} ?>
				</div>
			</div>
		</div>
	</section><!-- .spotlight -->

	<section id="one portfolio" class="style1 bottom s_portfolio new-style">
		<div class="content">
			<div class="container">
				<div class="rows">
					<h2><?php echo carbon_get_the_post_meta( 'title12' ); ?></h2>
					<div id="portfolio_grid">
						<?php
						$gallery = carbon_get_the_post_meta( 'crb_gallery' );
						foreach ( $gallery as $item ) { ?>
							<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2 category-3">

								<img src="<?php echo $item['img'] ?>" class="prev" alt="Alt">
								<div class="port_item_cont">
									<a href="#" class="popup_content"><img
												src="<?php echo get_template_directory_uri() . '/assets/img/lupa.png' ?>"></a>
								</div>
								<div class="hidden">
									<div class="podrt_descr">
										<div class="modal-box-content">
											<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
											<img src="<?php echo $item['img'] ?>" alt="Alt">
										</div>
									</div>
								</div>
							</div>
							<?php
						} ?>

					</div>
				</div>
			</div>
		</div>

	</section>


<?php
//get_template_part( 'template-parts/content', 'contact' );

get_footer( 'new' );
