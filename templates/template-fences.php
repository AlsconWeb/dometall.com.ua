<?php
/**
 * Template Name: Fences page
 */

get_header( 'new' );
?>

	<section id="first-screen" class="first-screen">
		<div class="top">
			<div class="container">
				<div class="content">
					<h1><?php echo carbon_get_the_post_meta( 'crb_title' ) ?></h1>
					<ul>
						<?php
						$list = carbon_get_the_post_meta( 'crb_lists' );
						foreach ( $list as $item ) {
							echo '<li>&nbsp&nbsp' . $item['text'] . '</li>';
						}
						?>
					</ul>

					<a href="#contact-modal" rel="modal:open" class="btn">Замовити</a>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="container">
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_first' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_first' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_second' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_second' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_third' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_third' ) ?></p>
				</div>
				<div class="column">
					<h4><?php echo carbon_get_the_post_meta( 'crb_title_fourth' ) ?></h4>
					<p><?php echo carbon_get_the_post_meta( 'crb_text_fourth' ) ?></p>
				</div>
			</div>
		</div>
	</section><!-- #first-screen -->

	<section id="second-screen" class="second-screen">
		<div class="container">
			<h3>ПЕРЕВАГИ РОБОТИ З НАМИ</h3>

			<div class="icons-list">
				<div class="icon-column">
					<div class="icon-box">
					</div>
					<p>Індивідуальна система знижок для кожного клієнта</p>
				</div>

				<div class="icon-column">
					<div class="icon-box">
					</div>
					<p>Максимально швидкі терміни виготовлення сходів</p>
				</div>

				<div class="icon-column">
					<div class="icon-box">
					</div>
					<p>Можливість безготівкової форми оплати</p>
				</div>

				<div class="icon-column">
					<div class="icon-box">
					</div>
					<p>12 місяців гарантії на вироби з металоконструкцій</p>
				</div>
			</div>
		</div>
	</section><!-- #second-screen -->

	<section id="one portfolio" class="spotlight style1 bottom s_portfolio">
		<div class="content">
			<div class="container">
				<div class="rows">
					<h3>НАШІ ОСТАННІ РОБОТИ</h3>
					<div id="portfolio_grid">

						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2 category-3">

							<img src="https://img.dometall.com.ua/pict6140_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/pict6140_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>

						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2">
							<img src="https://img.dometall.com.ua/PICT6795_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/PICT6795_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>

						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/PICT1355_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/PICT1355_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/IMG_6645_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/IMG_6645_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/IMG_3632_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/IMG_3632_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/PICT1038_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/PICT1038_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/IMG_2604_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/IMG_2604_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/14_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/14_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/IMG_1139_prev.JPG" class="prev" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/IMG_1139_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
		<a href="#one2" class="goto-next scrolly">Next</a>
	</section>


<?php
//get_template_part( 'template-parts/content', 'contact' );

get_footer( 'new' );
