<?php
/**
 * Template Name: Metal page
 */

get_header( 'new' );
?>

	<section id="one portfolio" class="style1 bottom s_portfolio new-style">

		<div class="content">
			<div class="container">
				<div class="rows">
					<h2>ФОТО НАШИХ МЕТАЛОКОНСТРУКЦІЙ</h2>
					<div id="portfolio_grid">
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2 category-3">

							<img src="https://img.dometall.com.ua/metallconstruct_1_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_1_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>

						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-2">
							<img src="https://img.dometall.com.ua/metallconstruct_2_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_2_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>

						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_3_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_3_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_4_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_4_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_5_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_5_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_6_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_6_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_7_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_7_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_8_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_8_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_9_prev.JPG" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_9_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_10_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_10_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_11_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_11_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_12_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_12_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_13_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_13_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_14_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_15_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_15_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_16_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_16_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_17_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_17_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_18_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_18_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_19_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_19_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_20_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
						<div class="mix col-md-4 col-sm-6 col-xs-12 portfolio_item category-1 category-3">
							<img src="https://img.dometall.com.ua/metallconstruct_20_prev.jpg" alt="Alt">
							<div class="port_item_cont">
								<a href="#" class="popup_content"><img src="https://img.dometall.com.ua/lupa.png"></a>
							</div>
							<div class="hidden">
								<div class="podrt_descr">
									<div class="modal-box-content">
										<button class="mfp-close" type="button" title="Закрыть (Esc)">×</button>
										<img src="https://img.dometall.com.ua/metallconstruct_21_main.jpg" alt="Alt">
										<!--p>Lorem ipsum dolor sit amet</p-->
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</section>

<?php
//get_template_part( 'template-parts/content', 'contact' );

get_footer( 'new' );